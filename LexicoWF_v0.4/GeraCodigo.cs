﻿using System;
using System.Collections.Generic;
using System.IO;
using SemanticoWF;
using SintaticoWF;

namespace GeraCodigoWF
{
    public class GeraCodigo
    {
        public static GeraCodigo geracodigo = new GeraCodigo();

        string codigoGerado = @"CodigoGerado.txt"; //geracod

        public void convertePost(ref List<string> infix)
        {
            bool temInv = false;
            Stack<string> s = new Stack<string>();
            List<string> outputList = new List<string>();
            outputList.Clear();
            int n;
            char g;
            foreach (string c in infix)
            {
                int ret;
                string aux_string = "";


                ret = Semantico.analiseSemantico.Pesquisa_tabela(c, Sintatico.analiseSintatico.nivelEscopo);

                if (ret != -1)
                {
                    aux_string = Semantico.analiseSemantico.tabelaSimbolos[ret].tipo;
                }

                if (int.TryParse(c.ToString(), out n) || char.TryParse(c.ToString(), out g) || aux_string == "inteiro" || aux_string == "booleano")
                {
                    if (!isOperator(c) && c != "(" && c != ")" || c == "I")
                    {
                        outputList.Add(c);
                    }
                }
                if (c == "(")
                {
                    s.Push(c);
                }
                if (c == ")")
                {
                    while (s.Count != 0 && s.Peek() != "(")
                    {
                        outputList.Add(s.Pop());
                    }
                    s.Pop();
                }
                if (isOperator(c) == true)
                {
                    while (s.Count != 0 && Priority(s.Peek()) >= Priority(c))
                    {
                        outputList.Add(s.Pop());
                    }
                    s.Push(c);
                }
            }
            while (s.Count != 0)//if any operators remain in the stack, pop all & add to output list until stack is empty 
            {
                outputList.Add(s.Pop());
            }
            for (int i = 0; i < outputList.Count; i++)
            {
                int ret;
                string aux_string = "";

                ret = Semantico.analiseSemantico.Pesquisa_tabela(outputList[i].ToString(), Sintatico.analiseSintatico.nivelEscopo);

                if (ret != -1)
                {
                    aux_string = Semantico.analiseSemantico.tabelaSimbolos[ret].tipo;
                }

                //MessageBox.Show(outputList[i].ToString());
                if (int.TryParse(outputList[i].ToString(), out n) || outputList[i].ToString() == "falso" || outputList[i].ToString() == "verdadeiro")
                {
                    if (outputList[i].ToString() == "falso")
                    {
                        Gera("", "LDC", "0", "");

                    }
                    else if (outputList[i].ToString() == "verdadeiro")
                    {
                        Gera("", "LDC", "1", "");

                    }
                    else
                    {
                        Gera("", "LDC", outputList[i].ToString(), "");
                        if (temInv)
                        {
                            Gera("", "INV", "", "");
                            temInv = false;
                        }
                    }



                }
                else if (char.TryParse(outputList[i].ToString(), out g) && outputList[i].ToString() != "falso" && outputList[i].ToString() != "verdadeiro" && outputList[i].ToString() != "I" && outputList[i].ToString() != "!=" && outputList[i].ToString() != "e" && outputList[i].ToString() != "ou" && outputList[i].ToString() != ">" && outputList[i].ToString() != "<" && outputList[i].ToString() != ">=" && outputList[i].ToString() != "<=" && outputList[i].ToString() != "-" && outputList[i].ToString() != "+" && outputList[i].ToString() != "=" && outputList[i].ToString() != "*" && outputList[i].ToString() != "div" || aux_string == "inteiro" || aux_string == "booleano")
                {
                    //ao inves de usar outputList[i].ToString() usar o lugar da memoria 
                    if (Sintatico.analiseSintatico.temFuncao == false)
                    {
                        int posMem;
                        posMem = Semantico.analiseSemantico.Pesquisa_memoria_tabela(outputList[i], Sintatico.analiseSintatico.nivelEscopo);
                        Gera("", "LDV", posMem.ToString(), "");
                        if (temInv)
                        {
                            Gera("", "INV", "", "");
                            temInv = false;
                        }

                    }
                    else
                    {
                        Sintatico.analiseSintatico.temFuncao = false;
                    }

                }
                else if (outputList[i].ToString() == ">")
                {
                    Gera("", "CMA", "", "");
                }
                else if (outputList[i].ToString() == "<")
                {
                    Gera("", "CME", "", "");
                }
                else if (outputList[i].ToString() == "!=")
                {
                    Gera("", "CDIF", "", "");
                }
                else if (outputList[i].ToString() == "=")
                {
                    Gera("", "CEQ", "", "");
                }
                else if (outputList[i].ToString() == "<=")
                {
                    Gera("", "CMEQ", "", "");
                }
                else if (outputList[i].ToString() == ">=")
                {
                    Gera("", "CMAQ", "", "");
                }
                else if (outputList[i].ToString() == "e")
                {
                    Gera("", "AND", "", "");
                }
                else if (outputList[i].ToString() == "ou")
                {
                    Gera("", "OR", "", "");
                }
                else if (outputList[i].ToString() == "+")
                {
                    Gera("", "ADD", "", "");
                }
                else if (outputList[i].ToString() == "-")
                {
                    Gera("", "SUB", "", "");
                }
                else if (outputList[i].ToString() == "*")
                {
                    Gera("", "MULT", "", "");
                }
                else if (outputList[i].ToString() == "div")
                {
                    Gera("", "DIVI", "", "");
                }
                else if (outputList[i].ToString() == "I")
                {
                    temInv = true;
                }

                //System.Console.WriteLine("{0}", outputList[i]);
            }
        }

        public int Priority(string c)
        {

            if (c == "I")
            {
                return 5;
            }
            else if (c == "*" || c == "div")
            {
                return 4;
            }
            else if (c == "+" || c == "-")
            {
                return 3;
            }
            else if (c == ">" || c == "<" || c == "<=" || c == ">=" || c == "=" || c == "!=")
            {
                return 2;
            }
            else if (c == "e")
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public void VerificaUnario()
        {
            int ret;
            for (int i = 0; i < Sintatico.analiseSintatico.auxExpressao.Count; i++)
            {
                if (Sintatico.analiseSintatico.auxExpressao[i] == "-")
                {
                    if (i != 0)
                    {
                        ret = Semantico.analiseSemantico.Pesquisa_tabela(Sintatico.analiseSintatico.auxExpressao[i - 1], Sintatico.analiseSintatico.nivelEscopo);

                        if (ret > -1 || int.TryParse(Sintatico.analiseSintatico.auxExpressao[i - 1], out int r))
                        {
                            //
                        }
                        else
                        {
                            Sintatico.analiseSintatico.auxExpressao[i] = "I";
                        }

                    }
                    else
                    {
                        if (Sintatico.analiseSintatico.auxExpressao[i] == "-")
                        {
                            Sintatico.analiseSintatico.auxExpressao[i] = "I";

                        }
                    }





                }
            }
        }

        public bool isOperator(string ch)
        {
            if (ch == "+" || ch == "-" || ch == "nao" || ch == "*" || ch == "div" || ch == ">" || ch == "<" || ch == "<=" || ch == ">=" || ch == "=" || ch == "!=" || ch == "e" || ch == "ou")
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        public void Gera(string s1, string s2, string s3, string s4)
        {
            string comando;

            if (s1 == "")
            {
                comando = s2 + ' ' + s3 + ' ' + s4;
            }
            else
            {
                comando = s1 + ' ' + s2 + ' ' + s3 + ' ' + s4;

            }

            if (!File.Exists(codigoGerado))
            {
                File.Create(codigoGerado);
                TextWriter tw = new StreamWriter(codigoGerado);
                tw.WriteLine(comando + "\n");
                tw.Close();
            }
            else if (File.Exists(codigoGerado))
            {
                using (var tw = new StreamWriter(codigoGerado, true))
                {
                    tw.WriteLine(comando + "\n");
                }
            }
        }

    }
}
