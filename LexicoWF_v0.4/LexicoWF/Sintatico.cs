﻿using LexicoWF;
using System;
using System.Collections.Generic;
using SemanticoWF;
using System.IO;
using System.Windows.Forms;
using System.Linq;
using GeraCodigoWF;


namespace SintaticoWF
{
    public class Sintatico
    {

        //Label de cada procedimento e funcao
        public int auxRot_global = 0;

        //Marca nome do ultimo procedimento
        public string ultimoProcedimento = "";

        //Label de cada rotulo
        public int rotulo; 

        //Guarda os allocs que foram feitos para serem desalocados depois
        public List<int> ultimoalloc = new List<int>(); 

        //Marca quantos allocs estao sendo feitos no procedimento
        public int procAlloc = 0;
        public int procAllocCount = 0;

        //Quantas variaveis serao alocadas
        public int totalVar = 0; 

        //Memoria de cada variavel
        public int espacoMemoria = 0;
        
        //Marca quantos procedimentos estao ativos
        public List<int> checaProcedimento = new List<int>();
       
        public int checaInicio = 0;
        public int numProcedimentos = 0;

        //Guarda a expressao atual do momento
        public static List<string> auxExpressao = new List<string>();

        //Marca = 1 quando esta dentro de uma funcao
        public int checaFuncao = 0;

        //Marca o nome do ultimo procedimento ou funcao que passou
        public string auxNome_procFunc = "";

        //Variaveis para ver em que nivel do escopo esta em determinado momento
        public static int nivelEscopo = 0;
        public int nivelAlloc = 0;

        //Auxiliares para alloc
        public int aux_espacoMem, aux_totalvar;
        
        //Marcacao de procedimento / func
        public static int marcaProc_Func = 0;


        //Flags
        public bool temAlloc = false;
        public bool checaGera = false;
        public bool expressaoFunc = false;
        public bool nivelIgual = true;
        public static bool verificaRetorno = false;
        public bool temCall = false;
        public static bool temFuncao = false;
        public int dentroFunc = 0;
        public int temAllocFunc = 0;
        public bool temProcedimento = false;
        public static bool geraStr_atrib = false;

        //Instancias de cada analise
        private Lexico analiseLexico;
        private Semantico analiseSemantico;
        private GeraCodigo geracodigo;
        public static Token token;

        //Necessario para controlar os datagrids de Erros e listagem de tokens
        public DataGridView dtview1, dtview2;

        public Sintatico(string conteudo, DataGridView sdt1, DataGridView sdt2)
        {
            dtview1 = sdt1;
            dtview2 = sdt2;

            analiseLexico = new Lexico();
            analiseLexico.AtribuiInterface(sdt1, sdt2);
            analiseSemantico = new Semantico(sdt1,sdt2, analiseLexico);
            geracodigo = new GeraCodigo(analiseSemantico);
            Main(conteudo);
        }


        public void Main(string conteudo)
        {
            
             rotulo = 1; 
            token = analiseLexico.AnalisadorLexical(conteudo);

            if (string.Equals(token.simbolo, "sprograma"))
            {
               geracodigo.Gera("", "START", "", ""); 
                auxNome_procFunc = token.lexema;
                token = analiseLexico.AnalisadorLexical(conteudo);

                if (string.Equals(token.simbolo, "sidentificador"))
                {
                    analiseSemantico.Insere_tabela(token.lexema, "programa", 1, espacoMemoria, auxNome_procFunc);
                    token = analiseLexico.AnalisadorLexical(conteudo);

                    if (string.Equals(token.simbolo, "sponto_virgula"))
                    {
                        Analisa_bloco(conteudo);
                        
                        if (string.Equals(token.simbolo, "sponto"))
                        {
                            TrataErroSintatico("Programa Terminou");

                           


                            temAlloc = true;
                            if (temAlloc)
                            {
                                analiseSemantico.Pesquisa_tabela_alloc(0);
                              

                            }


                            for (int i = 0; i <  analiseSemantico.aux_tabelaAllocs.Count; i++)
                            {

                               geracodigo.Gera("", "DALLOC",  analiseSemantico.aux_tabelaAllocs[i].inicio.ToString(),  analiseSemantico.aux_tabelaAllocs[i].fim.ToString());


                            }


                            temAlloc = false;
                             analiseSemantico.aux_tabelaAllocs.Clear();

                          

                           geracodigo.Gera("", "HLT", "", ""); 
                        }
                        else
                        {
                            TrataErroSintatico("Erro: esperado '.'");

                           
                        }
                    }
                    else
                    {
                        TrataErroSintatico("Erro: esperado ';'");
                        
                    }
                }
                else
                {
                    TrataErroSintatico("Erro: esperado 'identificador'");
                    
                }
            }
            else
            {
                TrataErroSintatico("Erro: esperado 'programa'");

                
            }

        }


        private void TrataErroSintatico(string mensagem)
        {
            if ( analiseSemantico.terminaProgramaSintatico == 0)
            {

                dtview2.Rows.Add(new object[] { token.numLinha, mensagem + token.lexema });
                analiseLexico.AtribuiInterface(dtview1, dtview2);
                analiseSemantico.terminaProgramaSintatico = 1;
                
            }
           

        }


        private void Analisa_bloco(string conteudo)
        {

            token = analiseLexico.AnalisadorLexical(conteudo);
            if ( analiseSemantico.terminaProgramaSintatico == 0)
                Analisa_et_variaveis(conteudo);
            if ( analiseSemantico.terminaProgramaSintatico == 0)
                Analisa_subrotinas(conteudo);
            if ( analiseSemantico.terminaProgramaSintatico == 0)
                Analisa_comandos(conteudo);

        }

        private void Analisa_et_variaveis(string conteudo)
        {
            if (string.Equals(token.simbolo, "svar") &&  analiseSemantico.terminaProgramaSintatico == 0)
            {
                totalVar = 0;   

                token = analiseLexico.AnalisadorLexical(conteudo);

                if (string.Equals(token.simbolo, "sidentificador") &&  analiseSemantico.terminaProgramaSintatico == 0)
                {
                    while (string.Equals(token.simbolo, "sidentificador") &&  analiseSemantico.terminaProgramaSintatico == 0)
                    {
                        Analisa_variaveis(conteudo);

                        if (string.Equals(token.simbolo, "sponto_virgula") &&  analiseSemantico.terminaProgramaSintatico == 0)
                        {
                            if (procAlloc == 1)
                            {
                                procAllocCount++;
                            }

                            temAlloc = true;
                            if (temCall)
                            {
                                
                                temCall = false;
                            }
                            if(dentroFunc == 1)
                            {
                                temAllocFunc = 1;
                            }
                           geracodigo.Gera("", "ALLOC", espacoMemoria.ToString(), totalVar.ToString()); 
                             analiseSemantico.Insere_tabela_alloc(espacoMemoria, totalVar, nivelAlloc);
                            aux_totalvar = totalVar;
                            ultimoalloc.Add(totalVar);
                            espacoMemoria = espacoMemoria + totalVar; //lembrar de fazer "menos" com DALLOC
                            aux_espacoMem = espacoMemoria;
                            totalVar = 0;
                            token = analiseLexico.AnalisadorLexical(conteudo);
                        }
                        else
                        {
                            TrataErroSintatico("Erro: esperado ';'");
                            
                        }
                    }
                }
                else
                {
                    TrataErroSintatico("Erro: esperado 'identificador'");
                   
                }

            }
        }


        private void Analisa_variaveis(string conteudo)
        {
            do
            {
                if (string.Equals(token.simbolo, "sidentificador") &&  analiseSemantico.terminaProgramaSintatico == 0)
                {
                    if (!analiseSemantico.Pesquisa_duplicvar_tabela(token.lexema, nivelEscopo))
                    {
                         analiseSemantico.Insere_tabela(token.lexema, "variavel", -1, aux_espacoMem, auxNome_procFunc);
                        aux_espacoMem++;
                        totalVar++;  
                        token = analiseLexico.AnalisadorLexical(conteudo);

                        if (string.Equals(token.simbolo, "svirgula") || string.Equals(token.simbolo, "sdoispontos") &&  analiseSemantico.terminaProgramaSintatico == 0)
                        {
                            if (string.Equals(token.simbolo, "svirgula") &&  analiseSemantico.terminaProgramaSintatico == 0)
                            {
                                token = analiseLexico.AnalisadorLexical(conteudo);

                                if (string.Equals(token.simbolo, "sdoispontos") &&  analiseSemantico.terminaProgramaSintatico == 0)
                                {
                                    TrataErroSintatico("Erro: esperado ':' após ','");
                                    
                                }
                            }
                        }

                        else
                        {
                            TrataErroSintatico("Erro: esperado ':' ou ','");
                            
                        }
                    }
                    else
                    {
                         analiseSemantico.TrataErroSemantico("Erro: Variavel duplicada encontrada"); 
                    }
                }

                else
                {
                    TrataErroSintatico("Erro: esperado 'identificador'");
                    
                }
            } while (!(string.Equals(token.simbolo, "sdoispontos")) &&  analiseSemantico.terminaProgramaSintatico == 0);

            token = analiseLexico.AnalisadorLexical(conteudo);
            Analisa_tipo(conteudo);
        }

        private void Analisa_tipo(string conteudo)
        {
            if (!(string.Equals(token.simbolo, "sinteiro")) && !(string.Equals(token.simbolo, "sbooleano")) &&  analiseSemantico.terminaProgramaSintatico == 0)
            {
                TrataErroSintatico("Erro: esperado 'inteiro' ou 'booleano'");
               
            }
            else
            {
                 analiseSemantico.Coloca_tipo_tabela(token.lexema);
                token = analiseLexico.AnalisadorLexical(conteudo);
            }
        }

        private void Analisa_comandos(string conteudo)
        {
            if (string.Equals(token.simbolo, "sinicio") &&  analiseSemantico.terminaProgramaSintatico == 0)
            {
                if (temProcedimento == false)
                {
                    if (nivelEscopo > 0 && auxRot_global > 0)
                    {
                       geracodigo.Gera(auxRot_global.ToString(), "NULL", "", ""); 

                    }
                    else
                    {
                        if (checaFuncao == 0 && checaGera == false)
                        {
                           geracodigo.Gera("0", "NULL", "", ""); //geracod, marca inicio main
                        }

                    }

                }
                else
                {

                    //  Gera("0", "NULL", "", ""); //geracod, marca inicio main


                }

                token = analiseLexico.AnalisadorLexical(conteudo);
                Analisa_comando_simples(conteudo);

                while (!(string.Equals(token.simbolo, "sfim")) &&  analiseSemantico.terminaProgramaSintatico == 0)
                {
                    if ( analiseSemantico.Pesquisa_funcao_tabela(token.lexema))
                    {
                        
                        token = analiseLexico.AnalisadorLexical(conteudo);

                        if (!(string.Equals(token.simbolo, "sfim")) &&  analiseSemantico.terminaProgramaSintatico == 0)
                        {
                            Analisa_comando_simples(conteudo);
                        }

                    }
                    else
                    {
                        if (string.Equals(token.simbolo, "sponto_virgula") &&  analiseSemantico.terminaProgramaSintatico == 0)
                        {
                           
                            token = analiseLexico.AnalisadorLexical(conteudo);



                            if (!(string.Equals(token.simbolo, "sfim")) &&  analiseSemantico.terminaProgramaSintatico == 0)
                            {
                                Analisa_comando_simples(conteudo);
                            }
                        }
                        else
                        {



                            TrataErroSintatico("Erro: esperado ';'");

                        }
                    }
                }

                if (checaProcedimento.Count > 0) //significa que tem um procedimento (pode ter um dentro de outro ou dentro de uma funcao
                {
                    numProcedimentos--;
                    if (numProcedimentos >= 0)
                    {
                        
                        if (checaFuncao == 1) //pode ser uma funcao abrangendo um procedimento ou uma funcao sozinha
                        {
 
                            analiseSemantico.Pesquisa_tabela_alloc(nivelAlloc);
                            nivelAlloc--;


                            if (ultimoProcedimento != "") //funcao abrangendo procedimento
                            {

                                bool entrouFor = false; //ve se teve Alloc para fazer Dalloc
                                for (int i = 0; i <  analiseSemantico.aux_tabelaAllocs.Count; i++)
                                {

                                   geracodigo.Gera("", "DALLOC",  analiseSemantico.aux_tabelaAllocs[i].inicio.ToString(),  analiseSemantico.aux_tabelaAllocs[i].fim.ToString());
                                    entrouFor = true;

                                }

                                if (entrouFor)
                                {
                                    espacoMemoria = espacoMemoria - ultimoalloc[ultimoalloc.Count - 1];
                                    aux_espacoMem = espacoMemoria;
                                    
                                    ultimoalloc.RemoveAt(ultimoalloc.Count - 1);
                                    procAlloc = 0;
                                     analiseSemantico.aux_tabelaAllocs.Clear();
                                    temAlloc = false;

                                }
                                checaProcedimento.RemoveAt(0);
                                temProcedimento = false;
                               geracodigo.Gera("", "RETURN", "", "");
                                ultimoProcedimento = "";

                            }
                            else //Funcao sozinha
                            {
                                bool entrouFor = false;
                                if (temAllocFunc == 1)
                                {
                                    for (int i = 0; i < analiseSemantico.aux_tabelaAllocs.Count; i++)
                                    {

                                        geracodigo.Gera("", "RETURNF", analiseSemantico.aux_tabelaAllocs[i].inicio.ToString(), analiseSemantico.aux_tabelaAllocs[i].fim.ToString());
                                        temAllocFunc = 0;
                                        dentroFunc = 0;
                                        checaFuncao = 0;
                                        entrouFor = true;


                                    }
                                }
                                else
                                {
                                    geracodigo.Gera("", "RETURNF", "", "");
                                    temAllocFunc = 0;
                                    dentroFunc = 0;
                                    checaFuncao = 0;
                                    entrouFor = true;

                                }

                                if (entrouFor)
                                {
                                    if (!verificaRetorno)
                                    {

                                         analiseSemantico.TrataErroSemantico("ERRO: Funcao sem retorno");
                                    }
                                    checaFuncao = 0;
                                    espacoMemoria = espacoMemoria - ultimoalloc[ultimoalloc.Count - 1];
                                    
                                    ultimoalloc.RemoveAt(ultimoalloc.Count - 1);
                                    procAlloc = 0;
                                     analiseSemantico.aux_tabelaAllocs.Clear();
                                    temAlloc = false;


                                }

                            }




                        }
                        else //procedimento sozinho
                        {

                             analiseSemantico.Pesquisa_tabela_alloc(nivelAlloc);
                            nivelAlloc--;


                            bool entrouFor = false;
                            for (int i = 0; i <  analiseSemantico.aux_tabelaAllocs.Count; i++)
                            {

                               geracodigo.Gera("", "DALLOC",  analiseSemantico.aux_tabelaAllocs[i].inicio.ToString(),  analiseSemantico.aux_tabelaAllocs[i].fim.ToString());
                                entrouFor = true;

                            }



                           
                            checaProcedimento.RemoveAt(0);
                            temProcedimento = false;

                           geracodigo.Gera("", "RETURN", "", "");
                            if (entrouFor)
                            {
                                espacoMemoria = espacoMemoria - ultimoalloc[ultimoalloc.Count - 1];
                                aux_espacoMem = espacoMemoria;

                                ultimoalloc.RemoveAt(ultimoalloc.Count - 1);
                                procAlloc = 0;
                                 analiseSemantico.aux_tabelaAllocs.Clear();
                                temAlloc = false;

                            }



                        }

                    }

                }
                else if (checaFuncao == 1) //funcao sozinha - se for uma funcao faz returnF
                {

                     analiseSemantico.Pesquisa_tabela_alloc(nivelAlloc);
                    nivelAlloc--;

                    bool entrouFor = false;
                    if (temAllocFunc == 1)
                    {
                        for (int i = 0; i < analiseSemantico.aux_tabelaAllocs.Count; i++)
                        {

                                geracodigo.Gera("", "RETURNF", analiseSemantico.aux_tabelaAllocs[i].inicio.ToString(), analiseSemantico.aux_tabelaAllocs[i].fim.ToString());
                                temAllocFunc = 0;
                                dentroFunc = 0;
                            checaFuncao = 0;
                            entrouFor = true;


                        }
                    }
                    else
                    {
                            geracodigo.Gera("", "RETURNF", "", "");
                        temAllocFunc = 0;
                        dentroFunc = 0;
                        checaFuncao = 0;
                        entrouFor = true;

                    }
                    if (entrouFor)
                    {
                        if (!verificaRetorno)
                        {
                             analiseSemantico.TrataErroSemantico("ERRO: Funcao sem retorno");
                        }
                        espacoMemoria = espacoMemoria - ultimoalloc[ultimoalloc.Count - 1];
                        
                        ultimoalloc.RemoveAt(ultimoalloc.Count - 1);
                        procAlloc = 0;
                         analiseSemantico.aux_tabelaAllocs.Clear();


                    }


                }

                token = analiseLexico.AnalisadorLexical(conteudo);
            }
            else
            {
                TrataErroSintatico("Erro: esperado 'inicio'");

                
            }

        }

        private void Analisa_comando_simples(string conteudo)
        {
            

            if (string.Equals(token.simbolo, "sidentificador") &&  analiseSemantico.terminaProgramaSintatico == 0)
            {
                Analisa_atrib_chprocedimento(conteudo);
            }
            else if (string.Equals(token.simbolo, "sse") &&  analiseSemantico.terminaProgramaSintatico == 0)
            {
                //checaGera nao deixa gerar um 0 NULL a nao ser que seja para a main
                checaGera = true;
                Analisa_se(conteudo);
                checaGera = false;
                
            }
            else if (string.Equals(token.simbolo, "senquanto") &&  analiseSemantico.terminaProgramaSintatico == 0)
            {

                checaGera = true;
                Analisa_enquanto(conteudo);
                checaGera = false;

            }
            else if (string.Equals(token.simbolo, "sleia") &&  analiseSemantico.terminaProgramaSintatico == 0)
            {
                checaGera = true;
                Analisa_leia(conteudo);
                checaGera = false;

            }
            else if (string.Equals(token.simbolo, "sescreva") &&  analiseSemantico.terminaProgramaSintatico == 0)
            {
                checaGera = true;
                Analisa_escreva(conteudo);
                checaGera = false;

            }
            else
            {
                if ( analiseSemantico.terminaProgramaSintatico == 0)
                    Analisa_comandos(conteudo);
            }
        }

        private void Analisa_atrib_chprocedimento(string conteudo)
        {
            string aux;
            aux = token.lexema;
            


            token = analiseLexico.AnalisadorLexical(conteudo);
            if (string.Equals(token.simbolo, "satribuicao") &&  analiseSemantico.terminaProgramaSintatico == 0)
            {
                Analisa_atribuicao(conteudo, aux);

                if (temFuncao == false) //Se feito atribuicao em uma funcao nao faz str
                {
                    int posMem;
                    posMem =  analiseSemantico.Pesquisa_memoria_tabela(aux, nivelEscopo);
                   geracodigo.Gera("", "STR", posMem.ToString(), "");
                }
                else
                {
                    temFuncao = false;
                }

                if(geraStr_atrib)
                {
                    int posMem;
                    posMem = analiseSemantico.Pesquisa_memoria_tabela(aux, nivelEscopo);
                    geracodigo.Gera("", "STR", posMem.ToString(), "");

                    geraStr_atrib = false;
                }
  

            }
            else
            {
               geracodigo.Gera("", "CALL", aux, ""); 
                                           
                temCall = true;
                
            }
        }

        private void Analisa_leia(string conteudo)
        {
            checaInicio++;
            token = analiseLexico.AnalisadorLexical(conteudo);
            if (string.Equals(token.simbolo, "sabre_parenteses") &&  analiseSemantico.terminaProgramaSintatico == 0)
            {
                token = analiseLexico.AnalisadorLexical(conteudo);
                if (string.Equals(token.simbolo, "sidentificador") &&  analiseSemantico.terminaProgramaSintatico == 0)
                {

                    if (analiseSemantico.Pesquisa_declvar_tabela(token.lexema))
                    {
                       geracodigo.Gera("", "RD", "", "");
                        

                        int posMem;
                        posMem =  analiseSemantico.Pesquisa_memoria_tabela(token.lexema, nivelEscopo);
                       geracodigo.Gera("", "STR", posMem.ToString(), "");


                        token = analiseLexico.AnalisadorLexical(conteudo);

                        if (string.Equals(token.simbolo, "sfecha_parenteses") &&  analiseSemantico.terminaProgramaSintatico == 0)
                        {
                            token = analiseLexico.AnalisadorLexical(conteudo);
                        }
                        else
                        {
                            TrataErroSintatico("Erro: esperado ')'");

                           
                        }
                    }
                    else
                    {
                         analiseSemantico.TrataErroSemantico("Erro: Variavel não declarada");
                    }
                }
                else
                {
                    TrataErroSintatico("Erro: esperado 'identificador'");

                   
                }
            }
            else
            {
                TrataErroSintatico("Erro: esperado '('");
                
            }
        }


        private void Analisa_escreva(string conteudo)
        {
            bool ret;
            checaInicio++;
            token = analiseLexico.AnalisadorLexical(conteudo);

            if (string.Equals(token.simbolo, "sabre_parenteses") &&  analiseSemantico.terminaProgramaSintatico == 0)
            {
                token = analiseLexico.AnalisadorLexical(conteudo);
                if (string.Equals(token.simbolo, "sidentificador") &&  analiseSemantico.terminaProgramaSintatico == 0)
                {
                    ret =  analiseSemantico.Pesquisa_funcao_tabela(token.lexema);

                    //Se for um escreva(funcao) faz um call se nao faz o LDV de qualquer forma faz um PRN depois
                    if (ret == false) 
                    {
                       
                        int posMem;
                        posMem =  analiseSemantico.Pesquisa_memoria_tabela(token.lexema, nivelEscopo);
                       geracodigo.Gera("", "LDV", posMem.ToString(), "");

                    }
                    else
                    {
                       geracodigo.Gera("", "CALL", token.lexema, "");
                       

                    }

                   geracodigo.Gera("", "PRN", "", "");


                    token = analiseLexico.AnalisadorLexical(conteudo);
                    if (string.Equals(token.simbolo, "sfecha_parenteses") &&  analiseSemantico.terminaProgramaSintatico == 0)
                    {
                        token = analiseLexico.AnalisadorLexical(conteudo);
                    }
                    else
                    {
                        TrataErroSintatico("Erro: esperado ')'");
                        
                    }
                }
                else
                {
                    TrataErroSintatico("Erro: esperado 'identificador'");

                   
                }
            }
            else
            {
                TrataErroSintatico("Erro: esperado '('");

                
            }

        }

        private void Analisa_enquanto(string conteudo)
        {
            checaInicio++;
            int auxrot1, auxrot2;          
            auxrot1 = rotulo;           
           geracodigo.Gera(rotulo.ToString(), "NULL", "", ""); 
            rotulo++;   
            string infix = "";
            auxExpressao.Clear();
            token = analiseLexico.AnalisadorLexical(conteudo);
            Analisa_expressao(conteudo);

           geracodigo.VerificaUnario();
            for (int index = 0; index < auxExpressao.Count(); index++)
            {
                
                infix = infix + auxExpressao[index];
            }

           geracodigo.convertePost(ref auxExpressao);


            if (! analiseSemantico.Verifica_booleano())
            {
                 analiseSemantico.TrataErroSemantico("ERRO: Expressao nao booleana");
            }


            if (string.Equals(token.simbolo, "sfaca") &&  analiseSemantico.terminaProgramaSintatico == 0)
            {
                auxrot2 = rotulo; //geracod
               geracodigo.Gera("", "JMPF", rotulo.ToString(), ""); //geracod
                rotulo++; //geracod

                token = analiseLexico.AnalisadorLexical(conteudo);

                Analisa_comando_simples(conteudo);
               geracodigo.Gera("", "JMP", auxrot1.ToString(), ""); //geracod
               geracodigo.Gera(auxrot2.ToString(), "NULL", "", ""); //geracod
            }
            else
            {
                TrataErroSintatico("Erro: esperado 'faca'");

                
            }
        }


      

        private void Analisa_se(string conteudo)
        {
            checaInicio++;
            string infix = "";
            int temSenao = 0;
            auxExpressao.Clear();
            token = analiseLexico.AnalisadorLexical(conteudo);
            expressaoFunc = true;
            Analisa_expressao(conteudo);
            expressaoFunc = false;
            int aux_rot1;

           geracodigo.VerificaUnario();

            for (int index = 0; index < auxExpressao.Count(); index++)
            {
               
                infix = infix + auxExpressao[index]; //Envia a auxExpressao para uma auxiliar que sera usada para faze a posfixa
            }

           geracodigo.convertePost(ref auxExpressao);

            if(analiseSemantico.Funcao_booleana())
            {
               // geracodigo.Gera("", "CALL", auxExpressao[0].ToString(), "");
               
                   // token = analiseLexico.AnalisadorLexical(conteudo);
            }

            if (! analiseSemantico.Verifica_booleano())
            {
                 analiseSemantico.TrataErroSemantico("ERRO: Expressao nao booleana");
            }


            if (string.Equals(token.simbolo, "sentao") &&  analiseSemantico.terminaProgramaSintatico == 0)
            {
                rotulo++;
                aux_rot1 = rotulo;
               geracodigo.Gera("", "JMPF", rotulo.ToString(), ""); //geracod
                token = analiseLexico.AnalisadorLexical(conteudo);
                Analisa_comando_simples(conteudo);



                if (string.Equals(token.simbolo, "ssenao"))
                {

                    rotulo++;
                   geracodigo.Gera("", "JMP", rotulo.ToString(), ""); //geracod

                    temSenao = 1;

                   geracodigo.Gera(aux_rot1.ToString(), "NULL", "", ""); //geracod


                    token = analiseLexico.AnalisadorLexical(conteudo);
                    Analisa_comando_simples(conteudo);

                   geracodigo.Gera(rotulo.ToString(), "NULL", "", ""); //geracod
                    rotulo++;

                    //rotulo = aux_rot1;
                }
            }
            else
            {
                TrataErroSintatico("Erro: esperado 'entao'");

            }

            if (temSenao == 0)
            {
               geracodigo.Gera(rotulo.ToString(), "NULL", "", ""); //geracod
                rotulo++;
            }


        }

        private void Analisa_subrotinas(string conteudo)
        {

            while (string.Equals(token.simbolo, "sprocedimento") || string.Equals(token.simbolo, "sfuncao") &&  analiseSemantico.terminaProgramaSintatico == 0)
            {


                if (string.Equals(token.simbolo, "sprocedimento") &&  analiseSemantico.terminaProgramaSintatico == 0)
                {
                    ultimoProcedimento = token.lexema;
                    procAlloc = 1;
                    numProcedimentos++;
                    nivelAlloc++;
                    Analisa_declaracao_procedimento(conteudo);
                }
                else
                {
                    nivelAlloc++;
                    checaFuncao = 1;
                    Analisa_declaracao_funcao(conteudo);
                }

                //token = analiseLexico.AnalisadorLexical(conteudo);
                if (string.Equals(token.simbolo, "sponto_virgula") &&  analiseSemantico.terminaProgramaSintatico == 0)
                {
                    token = analiseLexico.AnalisadorLexical(conteudo);
                }
                else
                {
                    TrataErroSintatico("Erro: esperado ';'");

                }
            }

    


        }

        private void Analisa_declaracao_procedimento(string conteudo)
        {
            dentroFunc = 0;
            nivelEscopo++;
            marcaProc_Func = 1;
            token = analiseLexico.AnalisadorLexical(conteudo);
            if (string.Equals(token.simbolo, "sidentificador") &&  analiseSemantico.terminaProgramaSintatico == 0)
            {
                if (! analiseSemantico.Pesquisa_declproc_tabela(token.lexema))
                {
                    if (nivelEscopo > 1) //Necessario para gerar um jmp para o local certo
                    {
                        auxNome_procFunc = token.lexema;
                         analiseSemantico.Insere_tabela(token.lexema, "procedimento", marcaProc_Func, espacoMemoria, auxNome_procFunc);
                        if (nivelIgual)
                        {
                            rotulo++;//geracod
                            nivelIgual = false;
                        }


                        auxRot_global = rotulo;
                       geracodigo.Gera("", "JMP", rotulo.ToString(), ""); //geracod VAI PRO INICIO DO PROGRAMA

                       geracodigo.Gera(token.lexema, "NULL", "", ""); //geracod
                        checaProcedimento.Add(1);
                        temProcedimento = true;
                        //rotulo--;//geracod
                    }
                    else //Se nao for maior que um entao gera o jump para a main
                    {
                        auxNome_procFunc = token.lexema;
                         analiseSemantico.Insere_tabela(token.lexema, "procedimento", marcaProc_Func, espacoMemoria, auxNome_procFunc);
                       geracodigo.Gera("", "JMP", "0", ""); //geracod VAI PRO INICIO DO PROGRAMA
                       geracodigo.Gera(token.lexema, "NULL", "", ""); //geracod
                        checaProcedimento.Add(1);

                        temProcedimento = true;
                        rotulo++;//geracod
                    }




                    token = analiseLexico.AnalisadorLexical(conteudo);
                    if (string.Equals(token.simbolo, "sponto_virgula") &&  analiseSemantico.terminaProgramaSintatico == 0)
                    {
                        Analisa_bloco(conteudo);
                    }
                    else
                    {
                        TrataErroSintatico("Erro: esperado ';'");

                    }
                }
                else
                {
                     analiseSemantico.TrataErroSemantico("ERRO: Procedimento já declarado");
                }

            }
            else
            {
                TrataErroSintatico("Erro: esperado 'identificador'");

            }
            nivelEscopo--;
            analiseSemantico.Desempilha_tabela(); //Desempilha ate chegar no procedimento com a marcação e retira a mesma
            marcaProc_Func = 0;

        }

        private void Analisa_declaracao_funcao(string conteudo)
        {
            dentroFunc = 1;
            verificaRetorno = false;
            nivelEscopo++;
            marcaProc_Func = 1;

            token = analiseLexico.AnalisadorLexical(conteudo);
            if (string.Equals(token.simbolo, "sidentificador") &&  analiseSemantico.terminaProgramaSintatico == 0)
            {
                if (! analiseSemantico.Pesquisa_declfuncao_tabela(token.lexema))
                {
                    if (nivelEscopo > 1) //Necessario para gerar um jmp para o local certo
                    {
                        auxNome_procFunc = token.lexema;
                         analiseSemantico.Insere_tabela(token.lexema, "", marcaProc_Func, espacoMemoria, auxNome_procFunc);
                        if (nivelIgual)
                        {
                            rotulo++;//geracod
                            nivelIgual = false;
                        }
                        auxRot_global = rotulo;
                       geracodigo.Gera("", "JMP", rotulo.ToString(), ""); //geracod 
                       geracodigo.Gera(token.lexema, "NULL", "", ""); //geracod
                                                            
                    }
                    else //Se nao for maior que um entao gera o jump para a main
                    {
                        auxNome_procFunc = token.lexema;
                         analiseSemantico.Insere_tabela(token.lexema, "", marcaProc_Func, espacoMemoria, auxNome_procFunc);
                       geracodigo.Gera("", "JMP", "0", ""); //geracod VAI PRO INICIO DO PROGRAMA
                       geracodigo.Gera(token.lexema, "NULL", "", ""); //geracod
                                                            
                        rotulo++;//geracod
                    }

                    token = analiseLexico.AnalisadorLexical(conteudo);
                    if (string.Equals(token.simbolo, "sdoispontos") &&  analiseSemantico.terminaProgramaSintatico == 0)
                    {

                        token = analiseLexico.AnalisadorLexical(conteudo);
                        if (string.Equals(token.simbolo, "sinteiro") || string.Equals(token.simbolo, "sbooleano") &&  analiseSemantico.terminaProgramaSintatico == 0)
                        {
                            if (token.simbolo == "sinteiro")
                            {
                                 Semantico.tabelaSimbolos[ Semantico.tabelaSimbolos.Count - 1].tipo = "funcao inteiro";
                            }
                            else
                            {
                                 Semantico.tabelaSimbolos[ Semantico.tabelaSimbolos.Count - 1].tipo = "funcao booleano";

                            }
                            token = analiseLexico.AnalisadorLexical(conteudo);
                            if (string.Equals(token.simbolo, "sponto_virgula"))
                            {
                                Analisa_bloco(conteudo);
                            }
                        }
                        else
                        {
                            TrataErroSintatico("Erro: esperado 'inteiro' ou 'booleano'");

                        }
                    }
                    else
                    {
                        TrataErroSintatico("Erro: esperado ':'");

                    }
                }
                else
                {
                     analiseSemantico.TrataErroSemantico("ERRO: Funcao ja declarada");
                }
            }
            else
            {
                TrataErroSintatico("Erro: esperado 'identificador'");

            }
            nivelEscopo--;
            analiseSemantico.Desempilha_tabela();
            marcaProc_Func = 0;
        }


        //auxExpressao.Add(token.lexema); vai adicionando nesse auxiliar toda a expressao do momento para fazer a posfixa depois
        private void Analisa_expressao(string conteudo)
        {
            
            Analisa_expressao_simples(conteudo);

            if (string.Equals(token.simbolo, "smaior") || string.Equals(token.simbolo, "smaiorig") || string.Equals(token.simbolo, "sig") || string.Equals(token.simbolo, "smenor") || string.Equals(token.simbolo, "smenorig") || string.Equals(token.simbolo, "sdif") &&  analiseSemantico.terminaProgramaSintatico == 0)
            {
                auxExpressao.Add(token.lexema);
                token = analiseLexico.AnalisadorLexical(conteudo);
                Analisa_expressao_simples(conteudo);
            }
        }

        private void Analisa_expressao_simples(string conteudo) //cuidado
        {
            if ( analiseSemantico.terminaProgramaSintatico == 0)
            {


                if (string.Equals(token.simbolo, "smais") || string.Equals(token.simbolo, "smenos") &&  analiseSemantico.terminaProgramaSintatico == 0)
                {
                    auxExpressao.Add(token.lexema);
                    token = analiseLexico.AnalisadorLexical(conteudo);
                }
                Analisa_termo(conteudo);
                while (string.Equals(token.simbolo, "smais") || string.Equals(token.simbolo, "smenos") || string.Equals(token.simbolo, "sou") &&  analiseSemantico.terminaProgramaSintatico == 0)
                {
                    auxExpressao.Add(token.lexema);
                    token = analiseLexico.AnalisadorLexical(conteudo);
                    Analisa_termo(conteudo);
                }
            }
        }

        private void Analisa_termo(string conteudo)
        {
            if ( analiseSemantico.terminaProgramaSintatico == 0)
            {
                Analisa_fator(conteudo);
                while (string.Equals(token.simbolo, "smult") || string.Equals(token.simbolo, "sdiv") || string.Equals(token.simbolo, "se") &&  analiseSemantico.terminaProgramaSintatico == 0)
                {
                    auxExpressao.Add(token.lexema);
                    token = analiseLexico.AnalisadorLexical(conteudo);
                    Analisa_fator(conteudo);
                }
            }
        }

        private void Analisa_fator(string conteudo)
        {
          
            if ( analiseSemantico.terminaProgramaSintatico == 0)
            {
                if (string.Equals(token.simbolo, "sidentificador") &&  analiseSemantico.terminaProgramaSintatico == 0)
                {

                    int ret;
                    ret =  analiseSemantico.Pesquisa_tabela(token.lexema, nivelEscopo);
                    if (ret != -1)
                    {
                        if ( Semantico.tabelaSimbolos[ret].tipo == "funcao inteiro" ||  Semantico.tabelaSimbolos[ret].tipo == "funcao booleano")
                        {
                            auxExpressao.Add(token.lexema);
                           geracodigo.Gera("", "CALL", token.lexema, "");
                           
                            temCall = true;

                            token = analiseLexico.AnalisadorLexical(conteudo);  
                          
                        }
                        else
                        {
                            auxExpressao.Add(token.lexema);
                            token = analiseLexico.AnalisadorLexical(conteudo); 
                        }
                    }
                    else
                    {
                        MessageBox.Show(token.lexema);
                         analiseSemantico.TrataErroSemantico("Identificador não declarado");

                    }

                }
                else if (string.Equals(token.simbolo, "snumero") &&  analiseSemantico.terminaProgramaSintatico == 0)
                {
                    auxExpressao.Add(token.lexema);
                    token = analiseLexico.AnalisadorLexical(conteudo);
                }
                else if (string.Equals(token.simbolo, "snao") &&  analiseSemantico.terminaProgramaSintatico == 0)
                {
                    auxExpressao.Add(token.lexema);
                    token = analiseLexico.AnalisadorLexical(conteudo);
                    Analisa_fator(conteudo);
                }
                else if (string.Equals(token.simbolo, "sabre_parenteses") &&  analiseSemantico.terminaProgramaSintatico == 0)
                {
                    auxExpressao.Add(token.lexema);
                    token = analiseLexico.AnalisadorLexical(conteudo);
                    Analisa_expressao(conteudo);
                    

                    if (string.Equals(token.simbolo, "sfecha_parenteses") &&  analiseSemantico.terminaProgramaSintatico == 0)
                    {
                        auxExpressao.Add(token.lexema);
                        token = analiseLexico.AnalisadorLexical(conteudo);
                    }
                    else
                    {
                        TrataErroSintatico("Erro: esperado ')'");

                        //  MessageBox.Show("Erro (Analisa_fator): Esperado fecha_parenteses");
                    }
                }
                else if (string.Equals(token.lexema, "verdadeiro") || string.Equals(token.lexema, "falso") &&  analiseSemantico.terminaProgramaSintatico == 0)
                {
                    auxExpressao.Add(token.lexema);
                    auxNome_procFunc = token.lexema;
                     analiseSemantico.Insere_tabela(token.lexema, "booleano", nivelEscopo, -1, auxNome_procFunc);
                    token = analiseLexico.AnalisadorLexical(conteudo);
                }
                else
                {
                    TrataErroSintatico("Erro: operador lógico extra");

                    //MessageBox.Show("Erro (Analisa_fator): Possivel erro com operador lógico extra");
                }
            }
        }

        private void Analisa_atribuicao(string conteudo, string aux)
        {
            string infix = "";
            int tipo = 0;
            auxExpressao.Clear();
            if ( analiseSemantico.terminaProgramaSintatico == 0)
            {
                token = analiseLexico.AnalisadorLexical(conteudo);
                Analisa_expressao(conteudo);


               geracodigo.VerificaUnario(); 

                for (int index = 0; index < auxExpressao.Count(); index++)
                {
                    //MessageBox.Show(auxExpressao[index]);
                    infix = infix + auxExpressao[index];
                }

               geracodigo.convertePost(ref auxExpressao); //posfix



                //Trata de maneira diferente se o que for atribuido é um inteiro ou um booleano
                //tipo = 1 - inteiro, tipo = 2 - booleano, tipo = -1 nao existe
                 tipo = analiseSemantico.Verifica_tipo_atribuicao(aux);

                if(tipo == 1)
                {
                    analiseSemantico.Insere_verificaTipo();
                    if (!analiseSemantico.Verifica_compatibilidade(aux))
                    {
                        analiseSemantico.TrataErroSemantico("ERRO: Incompatibilidade de tipos na atribuição");
                    }
                    

                }
                else if(tipo == 2)
                {
                    if(!analiseSemantico.Verifica_booleano())
                    {
                        analiseSemantico.TrataErroSemantico("ERRO: Incompatibilidade de tipos na atribuição");
                    }
                    else
                    {
                        int ret = analiseSemantico.Pesquisa_tabela(aux, nivelEscopo);
                        if(ret != -1)
                        {
                            if(Semantico.tabelaSimbolos[ret].tipo == "funcao booleano")
                            {
                                verificaRetorno = true;
                                temFuncao = true;
                            }
                        }
                    }

                }
                else if(tipo == -1)
                {
                    analiseSemantico.TrataErroSemantico("ERRO: Variavel não declarada");
                }


            }
        }
    }
}
