﻿using System;
using System.Collections.Generic;
using System.IO;
using SemanticoWF;
using SintaticoWF;

namespace GeraCodigoWF
{
    public class GeraCodigo
    {
        

        string codigoGerado = @"CodigoGerado.txt";
        private Semantico analiseSemantico;

        public GeraCodigo(Semantico sanaliseSemantico)
        {
            analiseSemantico = sanaliseSemantico; //pega a mesma instancia para usar alguns metodos dele
        }

        //Funcao da posfixa adaptada para os simbolos desta linguagem
        public void convertePost(ref List<string> infix)
        {
            int temInv = 0;
            int temNot = 0;
            Stack<string> s = new Stack<string>();
            List<string> outputList = new List<string>();
            outputList.Clear();
            int n;
            char g;
            foreach (string c in infix)
            {
                int ret;
                string aux_string = "";


                ret = analiseSemantico.Pesquisa_tabela(c, Sintatico.nivelEscopo);

                if (ret != -1)
                {
                    aux_string =  Semantico.tabelaSimbolos[ret].tipo;
                }

                if (int.TryParse(c.ToString(), out n) || char.TryParse(c.ToString(), out g) || aux_string == "inteiro" || aux_string == "booleano" || c == "nao") 
                {
                    if (!isOperator(c) && c != "(" && c != ")" || c == "I" || c == "nao")
                    {
                        outputList.Add(c);
                    }
                }
                if (c == "(")
                {
                    s.Push(c);
                }
                if (c == ")")
                {
                    while (s.Count != 0 && s.Peek() != "(")
                    {
                        outputList.Add(s.Pop());
                    }
                    s.Pop();
                }
                if (isOperator(c) == true)
                {
                    while (s.Count != 0 && Priority(s.Peek()) >= Priority(c))
                    {
                        outputList.Add(s.Pop());
                    }
                    s.Push(c);
                }
            }
            while (s.Count != 0)//if any operators remain in the stack, pop all & add to output list until stack is empty 
            {
                outputList.Add(s.Pop());
            }
            for (int i = 0; i < outputList.Count; i++)
            {
                int ret;
                string aux_string = "";

                ret = analiseSemantico.Pesquisa_tabela(outputList[i].ToString(), Sintatico.nivelEscopo);

                if (ret != -1)
                {
                    aux_string =  Semantico.tabelaSimbolos[ret].tipo;
                }

                //MessageBox.Show(outputList[i].ToString());
                if (int.TryParse(outputList[i].ToString(), out n) || outputList[i].ToString() == "falso" || outputList[i].ToString() == "verdadeiro")
                {
                    if (outputList[i].ToString() == "falso")
                    {
                        Gera("", "LDC", "0", "");

                    }
                    else if (outputList[i].ToString() == "verdadeiro")
                    {
                        Gera("", "LDC", "1", "");

                    }
                    else
                    {
                        Gera("", "LDC", outputList[i].ToString(), "");
                        if (temInv > 0)//variavel temInv incrementada quando o simbolo I for encontrado
                        {
                            for(int j=0;j< temInv;j++)
                            {
                                Gera("", "INV", "", "");

                            }
                            temInv = 0;
                        }
                        if (temNot > 0) //variavel temNot incrementada quando o simbolo nao for encontrado
                        {

                            for (int j = 0; j < temNot; j++)
                            {
                                Gera("", "NEG", "", "");

                            }

                            temNot = 0;
                        }
                    }

                   

                } //Verifica o simbolo e sua instrucao correspondente para assembly
                else if (char.TryParse(outputList[i].ToString(), out g) && outputList[i].ToString() != "falso" && outputList[i].ToString() != "verdadeiro" && outputList[i].ToString() != "I" && outputList[i].ToString() != "!=" && outputList[i].ToString() != "e" && outputList[i].ToString() != "ou" && outputList[i].ToString() != ">" && outputList[i].ToString() != "<" && outputList[i].ToString() != ">=" && outputList[i].ToString() != "<=" && outputList[i].ToString() != "-" && outputList[i].ToString() != "+" && outputList[i].ToString() != "=" && outputList[i].ToString() != "*" && outputList[i].ToString() != "div" && outputList[i].ToString() != "nao" || aux_string == "inteiro" || aux_string == "booleano")
                {
                    if (Sintatico.temFuncao == false)
                    {
                        int posMem;
                        posMem = analiseSemantico.Pesquisa_memoria_tabela(outputList[i], Sintatico.nivelEscopo);
                        Gera("", "LDV", posMem.ToString(), "");  
                        if (temInv > 0)
                        {
                            for (int j = 0; j < temInv; j++)
                            {
                                Gera("", "INV", "", "");

                            }
                            temInv = 0;
                        }
                        if (temNot > 0)
                        {

                            for (int j = 0; j < temNot; j++)
                            {
                                Gera("", "NEG", "", "");

                            }

                            temNot = 0;
                        }

                    }
                    else
                    {
                        Sintatico.temFuncao = false;
                    }

                }
                else if (outputList[i].ToString() == ">")
                {
                    Gera("", "CMA", "", "");
                }
                else if (outputList[i].ToString() == "<")
                {
                    Gera("", "CME", "", "");
                }
                else if (outputList[i].ToString() == "!=")
                {
                    Gera("", "CDIF", "", "");
                }
                else if (outputList[i].ToString() == "=")
                {
                    Gera("", "CEQ", "", "");
                }
                else if (outputList[i].ToString() == "<=")
                {
                    Gera("", "CMEQ", "", "");
                }
                else if (outputList[i].ToString() == ">=")
                {
                    Gera("", "CMAQ", "", "");
                }
                else if (outputList[i].ToString() == "e")
                {
                    Gera("", "AND", "", "");
                }
                else if (outputList[i].ToString() == "ou")
                {
                    Gera("", "OR", "", "");
                }
                else if (outputList[i].ToString() == "+")
                {
                    Gera("", "ADD", "", "");
                }
                else if (outputList[i].ToString() == "-")
                {
                    Gera("", "SUB", "", "");
                }
                else if (outputList[i].ToString() == "*")
                {
                    Gera("", "MULT", "", "");
                }
                else if (outputList[i].ToString() == "div")
                {
                    Gera("", "DIVI", "", "");
                }
                else if (outputList[i].ToString() == "I")
                {
                    temInv++;
                }
                else if (outputList[i].ToString() == "nao")
                {
                    temNot++;

                }

                //System.Console.WriteLine("{0}", outputList[i]);
            }
        }

        //Ordem de prioridade para os simbolos da posfixa
        public  int Priority(string c)
        {

            if (c == "I" || c == "nao")
            {
                return 5;
            }
            else if (c == "*" || c == "div")
            {
                return 4;
            }
            else if (c == "+" || c == "-")
            {
                return 3;
            }
            else if (c == ">" || c == "<" || c == "<=" || c == ">=" || c == "=" || c == "!=")
            {
                return 2;
            }
            else if (c == "e")
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        // Verifica a expressao e substitui o sinal de - caso eles esteja acompanhado de uma var a direita e coloca I para verificação na prioridade da posfixa
        public void VerificaUnario()
        {
            int ret;
            for (int i = 0; i < Sintatico.auxExpressao.Count; i++)
            {
                if (Sintatico.auxExpressao[i] == "-")
                {
                    if (i != 0)
                    {
                        ret = analiseSemantico.Pesquisa_tabela(Sintatico.auxExpressao[i - 1], Sintatico.nivelEscopo);

                        if (ret > -1 || int.TryParse(Sintatico.auxExpressao[i - 1], out int r))
                        {
                            //
                        }
                        else
                        {
                            Sintatico.auxExpressao[i] = "I";
                        }

                    }
                    else
                    {
                        if (Sintatico.auxExpressao[i] == "-")
                        {
                            Sintatico.auxExpressao[i] = "I";

                        }
                    }





                }
            }

        }


       public  bool isOperator(string ch)
        {
            if (ch == "+" || ch == "-"  || ch == "*" || ch == "div" || ch == ">" || ch == "<" || ch == "<=" || ch == ">=" || ch == "=" || ch == "!=" || ch == "e" || ch == "ou")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //Funcao gera que escreve no arquivo
        public  void Gera(string s1, string s2, string s3, string s4)
        {
            string comando;

            if (s1 == "")
            {
                comando = s2 + ' ' + s3 + ' ' + s4;
            }
            else
            {
                comando = s1 + ' ' + s2 + ' ' + s3 + ' ' + s4;

            }

            if (!File.Exists(codigoGerado))
            {
                File.Create(codigoGerado);
                TextWriter tw = new StreamWriter(codigoGerado);
                tw.WriteLine(comando + "\n");
                tw.Close();
            }
            else if (File.Exists(codigoGerado))
            {
                using (var tw = new StreamWriter(codigoGerado, true))
                {
                    tw.WriteLine(comando + "\n");
                }
            }
        }

    }
}
