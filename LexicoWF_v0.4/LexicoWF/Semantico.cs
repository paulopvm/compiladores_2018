﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using SintaticoWF;
using GeraCodigoWF;
using LexicoWF;
using SimboloWF;

namespace SemanticoWF
{
    public class Semantico

    {

        //Tabela de simbolos
        public static List<Simbolo> tabelaSimbolos = new List<Simbolo>(); //semantico

        //Allocs
        public List<Alloc> tabelaAllocs = new List<Alloc>(); //semantico
        public List<Alloc> aux_tabelaAllocs = new List<Alloc>(); //semantico
   
        //Auxiliar para verificar tipos de atribuicao e se
        public List<Simbolo> verificaTipo = new List<Simbolo>();

        //Necessario para mexer na interface grafica
        public DataGridView dtview1, dtview2;

        //monitora se deve parar o programa
        public int terminaProgramaSintatico = 0;

        private Lexico analiseLexico;

        //Metodos Semanticos - INICIO

        public Semantico(DataGridView sdt1, DataGridView sdt2, Lexico slexico)
        {
            analiseLexico = slexico;
            dtview1 = sdt1;
            dtview2 = sdt2;
            tabelaSimbolos.Clear();
            tabelaAllocs.Clear();
            aux_tabelaAllocs.Clear();
            verificaTipo.Clear();
        }
 
        public void Insere_tabela(string slexema, string stipo, int snivel, int smemoria, string snomeaux)
        {

            tabelaSimbolos.Add(new Simbolo() { lexema = slexema, tipo = stipo, nivel = snivel, memoria = smemoria, nomeaux = snomeaux });

            System.Console.WriteLine("Lexema {0}, Memoria {1}\n", tabelaSimbolos[tabelaSimbolos.Count - 1].lexema, tabelaSimbolos[tabelaSimbolos.Count - 1].memoria);

        }

        public void Insere_tabela_alloc(int sinicio, int sfim, int snivel)
        {
            tabelaAllocs.Add(new Alloc() { inicio = sinicio, fim = sfim, nivel = snivel });

        }

        public bool Pesquisa_duplicvar_tabela(string slexema, int snivel)
        {
            if (PesquisaTabela(slexema, snivel))
            {
                return true; //duplicado
            }
            else
            {
                return false;
            }
        }

        public bool PesquisaTabela(string slexema, int snivel)
        {
            bool duplicada = false;
            

                int i = tabelaSimbolos.Count - 1;
                while (tabelaSimbolos[i].nivel != 1 && i>0) //Vai ate um procedimento marcado com 1(a marca)
                {
                    if (tabelaSimbolos[i].lexema == slexema)
                    {
                        duplicada = true;
                        break;
                    }

 
                    
                    i--;
                    
                    
                }
            //}
            return duplicada;
        }

        public void TrataErroSemantico(string mensagem)
        {
            if (terminaProgramaSintatico == 0)
            {
                dtview2.Rows.Add(new object[] { Sintatico.token.numLinha, mensagem});
                analiseLexico.AtribuiInterface(dtview1, dtview2);
                terminaProgramaSintatico = 1;

            }
        }

        public void Coloca_tipo_tabela(string slexema)
        {
            //Procurar na tabela de simbolos variaveis com o "tipo variavel" e substituir pelo seu tipo

            for (int i = tabelaSimbolos.Count - 1; i > 0; i--)
            {
                if (tabelaSimbolos[i].tipo == "variavel")
                {
                    if (slexema == "ooleano") //Lexico pegando ooleano - ultima hora
                    {
                        slexema = "booleano";
                    }

                    if (slexema == "nteiro")
                    {
                        slexema = "inteiro";
                    }
                    tabelaSimbolos[i].tipo = slexema;

                }

            }

        }

        public bool Pesquisa_declvar_tabela(string slexema)
        {
            //Pesquisa elementos que possuam tipo inteiro ou booleano, e compara o lexema desse tipo com o lexema a se comparar



            int i = tabelaSimbolos.Count - 1;
            while ( i > 0) // ve a tabela inteira, ja terao sido desempilhados as vars fora do escopo
            {
                if (tabelaSimbolos[i].lexema == slexema)
                {
                    return true;

                }

                i--;


            }

            return false;
        }

  
        public int Pesquisa_tabela(string slexema, int snivel)
        {
            

                int i = tabelaSimbolos.Count - 1;
                while (i > 0)
                {
                    if (tabelaSimbolos[i].lexema == slexema)
                    {
                        return i; //retorna o indice do que se esta pesquisando
                        
                    }
  
                    i--;


                }

            return -1;


        }

        public void Pesquisa_tabela_alloc(int snivel)
        {
            for (int i = tabelaAllocs.Count - 1; i >= 0; i--)
            {
                if (tabelaAllocs[i].nivel == snivel) //Allocs de acordo com cada procedimento ou funcao
                {
                    aux_tabelaAllocs.Add(new Alloc() { inicio = tabelaAllocs[i].inicio, fim = tabelaAllocs[i].fim, nivel = 0 });

                    tabelaAllocs.RemoveAt(i);
                }

            }
        }

        //Ve se tem funcao duplicada
        public bool Pesquisa_funcao_tabela(string slexema)
        {
            for (int i = tabelaSimbolos.Count - 1; i > 0; i--)
            {
                if (tabelaSimbolos[i].lexema == slexema && (tabelaSimbolos[i].tipo == "funcao inteiro" || tabelaSimbolos[i].tipo == "funcao booleano"))
                {
                    System.Console.WriteLine("Lexema{0} Memoria {1}", tabelaSimbolos[i].lexema, tabelaSimbolos[i].memoria);
                    return true;

                }

            }
            return false;
        }

        public int Pesquisa_memoria_tabela(string slexema, int snivel)
        {


            int i = tabelaSimbolos.Count - 1;
            while (i > 0) //ve tudo
            {
                if (tabelaSimbolos[i].lexema == slexema)
                {
                    return tabelaSimbolos[i].memoria; //Retorna a memoria da var pesquisada

                }

                i--;


            }

            return -1;
        }

        




        //Retorna se tem funcao na tabela
        public bool Pesquisa_declfuncao_tabela(string slexema)
        {
  
            for (int i = tabelaSimbolos.Count - 1; i > 0; i--)
            {
                if (tabelaSimbolos[i].lexema == slexema)
                {
                    return true;
                }

            }

            return false;
        }

        //ve se tem proc duplicado
        public bool Pesquisa_declproc_tabela(string slexema)
        {
            for (int i = tabelaSimbolos.Count - 1; i > 0; i--)
            {
                if (tabelaSimbolos[i].lexema == slexema && tabelaSimbolos[i].tipo == "procedimento")
                {
                    return true;
                }

            }

            return false;
        }

        //ve se o que vai receber o valor é uma funcao booleana
        public bool Funcao_booleana()
        {
            if(Sintatico.auxExpressao.Count == 1)
            {
                int ret;

                ret = Pesquisa_tabela(Sintatico.auxExpressao[0], Sintatico.nivelEscopo);
                if(ret!=-1)
                {
                    if(tabelaSimbolos[ret].tipo == "funcao booleano")
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        

        //Chama funcoes diferentes se o que for receber for um inteiro ou booleano
        public int Verifica_tipo_atribuicao (string aux)
        {
            int ret;
            ret = Pesquisa_tabela(aux, Sintatico.nivelEscopo);
            if(ret!=-1)
            {
                if (tabelaSimbolos[ret].tipo == "inteiro" || tabelaSimbolos[ret].tipo == "funcao inteiro")
                {
                    if (tabelaSimbolos[ret].tipo == "funcao inteiro")
                    {
                       // Sintatico.temFuncao = true;
                    }
                    return 1;
                }
                else if(tabelaSimbolos[ret].tipo == "booleano" || tabelaSimbolos[ret].tipo == "funcao booleano")
                {
                    return 2;
                }

            }

            return -1;
      
        }

        //Chama essa func quando para tipos 1
        public void Insere_verificaTipo()
        {

            
            for (int i = 0; i < Sintatico.auxExpressao.Count; i++)
            {
                if (Sintatico.auxExpressao[i] != "!=" && Sintatico.auxExpressao[i] != "e" && Sintatico.auxExpressao[i] != "ou" && Sintatico.auxExpressao[i] != ">" && Sintatico.auxExpressao[i] != "<" && Sintatico.auxExpressao[i] != ">=" && Sintatico.auxExpressao[i] != "<=" && Sintatico.auxExpressao[i] != "-" && Sintatico.auxExpressao[i] != "+" && Sintatico.auxExpressao[i] != "*" && Sintatico.auxExpressao[i] != "div")
                {
                    if (int.TryParse(Sintatico.auxExpressao[i], out int s))
                    {
                        verificaTipo.Add(new Simbolo() { lexema = Sintatico.auxExpressao[i], tipo = "inteiro", nivel = -1, memoria = -1 });

                    }
                    else
                    {
                        int ret;
                        ret = Pesquisa_tabela(Sintatico.auxExpressao[i], Sintatico.nivelEscopo);

                        if (ret != -1)
                        {
                            if (tabelaSimbolos[ret].tipo == "inteiro" || tabelaSimbolos[ret].tipo == "booleano" || tabelaSimbolos[ret].tipo == "funcao inteiro" || tabelaSimbolos[ret].tipo == "funcao booleano")
                            {
                                verificaTipo.Add(new Simbolo() { lexema = tabelaSimbolos[ret].lexema, tipo = tabelaSimbolos[ret].tipo, nivel = -1, memoria = -1 });

                            }
                        }

                    }
                }

            }




        }
        // tipos 1
        public bool Verifica_compatibilidade(string slexema) 
        {
            int ret;

            bool compativel = false;
            ret = Pesquisa_tabela(slexema, Sintatico.nivelEscopo);
            if (ret != -1)
            {
                for (int i = 0; i < verificaTipo.Count; i++)
                {
                    if (tabelaSimbolos[ret].tipo == verificaTipo[i].tipo || ((int.TryParse(verificaTipo[i].lexema, out int g)) && (tabelaSimbolos[ret].tipo == "inteiro")) || (tabelaSimbolos[ret].tipo == "inteiro" && verificaTipo[i].tipo == "funcao inteiro") || (tabelaSimbolos[ret].tipo == "funcao inteiro" && verificaTipo[i].tipo == "inteiro") || (tabelaSimbolos[ret].tipo == "booleano" && verificaTipo[i].tipo == "funcao booleano") || (tabelaSimbolos[ret].tipo == "funcao booleano" && verificaTipo[i].tipo == "booleano"))
                    {
                        if (tabelaSimbolos[ret].tipo == "funcao inteiro" || tabelaSimbolos[ret].tipo == "funcao booleano")
                        {
                            Sintatico.verificaRetorno = true;
                        }

                        if ((tabelaSimbolos[ret].tipo == "inteiro" && verificaTipo[i].tipo == "funcao inteiro") || (tabelaSimbolos[ret].tipo == "funcao inteiro" && verificaTipo[i].tipo == "inteiro"))
                        {
                            Sintatico.temFuncao = true;
                            if((tabelaSimbolos[ret].tipo == "inteiro" && verificaTipo[i].tipo == "funcao inteiro"))
                            {
                                Sintatico.geraStr_atrib = true;
                            }
                        }
                        compativel = true;
                    }
                    else
                    {
                        compativel = false;
                    }
                }
                verificaTipo.Clear();
                return compativel;
            }
            else
            {
                TrataErroSemantico("ERRO: Variavel não declarada");
            }
            verificaTipo.Clear();
            return compativel;


        }

        public bool Verifica_booleano() //Verificar inteiro com operdor e / ou
        {
            int ret, indice;
            string expressao = "";
            bool compativel = false, opLogico = false, continua = true, continuaBool = true;
            List<string> aux1 = new List<string>();
            List<bool> listaBool = new List<bool>();

            indice = 0;

            for (int i = 0; i < Sintatico.auxExpressao.Count; i++)
            {

                if (Sintatico.auxExpressao[i] != "e" && Sintatico.auxExpressao[i] != "ou")
                    aux1.Add(Sintatico.auxExpressao[i]);




                if (Sintatico.auxExpressao[i] == "e" || Sintatico.auxExpressao[i] == "ou" || i + 1 == Sintatico.auxExpressao.Count)
                {
                    listaBool.Add(RetornaTipo_Expressao(aux1)); // Separa a expressao por e / ous e compara cada tipo retornado
                    aux1.Clear();

                }
            }


            for (int i = 0; i < listaBool.Count; i++)
            {

                if (listaBool[i] == true) // compara cada tipo retornado
                {
                    compativel = true;
                }
                else
                {
                    compativel = false;
                    break;
                }

            }

            if(Sintatico.auxExpressao[0] == "verdadeiro" || Sintatico.auxExpressao[0] == "falso") // booleano recebndo verdadeiro ou falso
            {
                compativel = true;
            }
                    

            return compativel;

        }

        public bool RetornaTipo_Expressao(List<string> auxExpressao)
        {
            int ret, indice;
            bool compativel = false, opLogico = false, continua = true, continuaBool = true;
            List<string> aux1 = new List<string>();
            //indice = 0;

            if (auxExpressao.Count == 1 || (auxExpressao.Count == 3 && (auxExpressao[0] == "(" && auxExpressao[2] == ")")))
            {

                if (auxExpressao.Count == 1)
                {
                    ret = Pesquisa_tabela(auxExpressao[0], Sintatico.nivelEscopo);
                    if (ret != -1)
                    {
                        if (tabelaSimbolos[ret].tipo == "booleano" || tabelaSimbolos[ret].tipo == "funcao booleano")
                        {
                            compativel = true;
                        }
                        else
                        {
                            compativel = false;
                        }
                    }

                }
                else
                {
                    ret = Pesquisa_tabela(auxExpressao[1], Sintatico.nivelEscopo);
                    if (ret != -1)
                    {
                        if (tabelaSimbolos[ret].tipo == "booleano" || tabelaSimbolos[ret].tipo == "ooleano")
                        {
                            compativel = true;
                        }
                        else
                        {
                            compativel = false;
                        }
                    }
                }


            }
            else
            {
                
                for (int i = 0; i < auxExpressao.Count; i++) // trata caso tipo a+z com z booleano
                {
                    if (auxExpressao[i] == "-" || auxExpressao[i] == "+" || auxExpressao[i] == "*" || auxExpressao[i] == "div")
                    {
                        if (!isOperator(auxExpressao[i - 1]) && !isOperator(auxExpressao[i + 1])) // ex a+b
                        {
                            ret = Pesquisa_tabela(auxExpressao[i - 1], Sintatico.nivelEscopo);
                            if (ret != -1)
                            {
                                if (tabelaSimbolos[ret].tipo == "booleano" || tabelaSimbolos[ret].tipo == "ooleano")
                                {
                                    continuaBool = false;
                                    break;
                                }
                            }

                            ret = Pesquisa_tabela(auxExpressao[i + 1], Sintatico.nivelEscopo);
                            if (ret != -1)
                            {
                                if (tabelaSimbolos[ret].tipo == "booleano" || tabelaSimbolos[ret].tipo == "ooleano")
                                {
                                    continuaBool = false;
                                    break;
                                }
                            }

                        }

                    }
                    else
                    {
                        continuaBool = true;
                    }
                }



                for (int i = 0; i < auxExpressao.Count; i++)
                {
                    if (auxExpressao[i] == "e" || auxExpressao[i] == "ou")
                    {
                        opLogico = true;
                        break;
                    }
                    else
                    {
                        opLogico = false;
                    }
                }

                if (opLogico)
                {
                    for (int i = 0; i < auxExpressao.Count; i++)
                    {
                        ret = Pesquisa_tabela(auxExpressao[i], Sintatico.nivelEscopo);
                        if (ret != -1)
                        {
                            if (tabelaSimbolos[ret].tipo == "inteiro" || tabelaSimbolos[ret].tipo == "funcao inteiro")
                            {
                                continua = false;
                                compativel = false;
                                break;
                            }
                            else
                            {
                                continua = true;
                                compativel = true;
                            }


                        }
                        else
                        {
                            if (int.TryParse(auxExpressao[i], out int g))
                            {
                                continua = false;
                                compativel = false;
                                break;
                            }
                        }

                    }
                }

                if (continua && continuaBool)
                {
                    for (int i = 0; i < auxExpressao.Count; i++)
                    {


                        ret = Pesquisa_tabela(auxExpressao[i], Sintatico.nivelEscopo);
                        if (ret != -1 || int.TryParse(auxExpressao[i], out int g))
                        {

                            if (ret != -1)
                            {
                                if (tabelaSimbolos[ret].tipo == "inteiro" || tabelaSimbolos[ret].tipo == "funcao inteiro" || tabelaSimbolos[ret].tipo == "booleano" || tabelaSimbolos[ret].tipo == "funcao booleano")
                                {
                                    compativel = true;
                                }
                                else
                                {
                                    compativel = false;
                                    break;
                                }
                            }
                            else
                            {

                                if (int.TryParse(auxExpressao[i], out int j))
                                {
                                    compativel = true;
                                }
                                else
                                {
                                    compativel = false;
                                    break;
                                }
                            }

                        }


                    }

                    if (compativel)
                    {
                        for (int i = 0; i < auxExpressao.Count; i++)
                        {
                            if (Verifica_operador_booleano(auxExpressao[i]))
                            {
                                compativel = true;
                                break;
                            }
                            else
                            {
                                compativel = false;
                            }

                        }
                    }

                }

            }

            return compativel;

        }

        public bool Verifica_operador_booleano(string ch)
        {
            if (ch == ">" || ch == "<" || ch == "<=" || ch == ">=" || ch == "=" || ch == "!=" || ch == "e" || ch == "ou" || ch =="nao")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool isOperator(string ch)
        {
            if (ch == "+" || ch == "-" || ch == "nao" || ch == "*" || ch == "div" || ch == ">" || ch == "<" || ch == "<=" || ch == ">=" || ch == "=" || ch == "!=" || ch == "e" || ch == "ou")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Desempilha_tabela() //desempilha ate  a marcacao
        {
            for (int i = tabelaSimbolos.Count - 1; i > 0; i--)
            {
                if(tabelaSimbolos[i].tipo == "procedimento" || tabelaSimbolos[i].tipo == "funcao inteiro" || tabelaSimbolos[i].tipo == "funcao booleano")
                {
                    if(tabelaSimbolos[i].nivel == 1)
                    {
                        tabelaSimbolos[i].nivel = 0;
                        break;
                    }
                    else
                    {
                        tabelaSimbolos.RemoveAt(i);
                    }

                }
                else
                {
                    tabelaSimbolos.RemoveAt(i);

                }
            }
        }


    }




}