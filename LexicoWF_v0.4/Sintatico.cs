﻿using LexicoWF;
using System;
using System.Collections.Generic;
using SemanticoWF;
using System.IO;
using System.Windows.Forms;
using System.Linq;
using GeraCodigoWF;

namespace SintaticoWF
{
    public class Sintatico
    {
        //Verificar compatibilidade de tipos
        //inicio Sint
        public static Sintatico analiseSintatico = new Sintatico();

        public int auxRot_global = 0;
        public string ultimoProcedimento = "";
        public bool temCall = false;
        public bool temFuncao = false;
        // public Lexico analiseLex = new Lexico();
        // public DataGridView dt_erros;
        public int rotulo; //geracod
        private Token token;
        public List<int> ultimoalloc = new List<int>(); //geracod
        public int procAlloc = 0;
        public int procAllocCount = 0;
        public int totalVar = 0; //geracod
        public int espacoMemoria = 0; //geracod                            //public int checaProcedimento = 0; //geracod
        public List<int> checaProcedimento = new List<int>();
        public bool temProcedimento = false;
        public int checaInicio = 0;
        public int numProcedimentos = 0;
        public List<string> auxExpressao = new List<string>();
        public int checaFuncao = 0;
        public string auxNome_procFunc = "";
        public bool temAlloc = false;
        //  public Semantico semantico = new Semantico();
        public int nivelEscopo = 0;
        public int nivelAlloc = 0;
        public int aux_espacoMem, aux_totalvar;
        bool checaGera = false;
        public bool nivelIgual = true;
        public bool verificaRetorno = false;



        public void Main(string conteudo)
        {

            //dt_erros = dt2;
            rotulo = 1; //geracod
            token = Lexico.analiseLexico.AnalisadorLexical(conteudo);

            if (string.Equals(token.simbolo, "sprograma"))
            {
                GeraCodigo.geracodigo.Gera("", "START", "", ""); //geracod
                auxNome_procFunc = token.lexema;
                token = Lexico.analiseLexico.AnalisadorLexical(conteudo);

                if (string.Equals(token.simbolo, "sidentificador"))
                {
                    Semantico.analiseSemantico.Insere_tabela(token.lexema, "programa", nivelEscopo, espacoMemoria, auxNome_procFunc);
                    token = Lexico.analiseLexico.AnalisadorLexical(conteudo);

                    if (string.Equals(token.simbolo, "sponto_virgula"))
                    {
                        Analisa_bloco(conteudo);
                        //token = Lexico.analiseLexico.AnalisadorLexical(conteudo);
                        if (string.Equals(token.simbolo, "sponto"))
                        {
                            TrataErroSintatico("Programa Terminou");

                            //espacoMemoria = espacoMemoria - ultimoalloc[ultimoalloc.Count - 1]; //comentado saida


                            temAlloc = true;
                            if (temAlloc)
                            {
                                Semantico.analiseSemantico.Pesquisa_tabela_alloc(0);
                                //nivelAlloc--;

                            }


                            for (int i = 0; i < Semantico.analiseSemantico.aux_tabelaAllocs.Count; i++)
                            {

                                GeraCodigo.geracodigo.Gera("", "DALLOC", Semantico.analiseSemantico.aux_tabelaAllocs[i].inicio.ToString(), Semantico.analiseSemantico.aux_tabelaAllocs[i].fim.ToString());


                            }


                            temAlloc = false;
                            Semantico.analiseSemantico.aux_tabelaAllocs.Clear();

                            //GeraCodigo.geracodigo.("", "DALLOC", espacoMemoria.ToString(), ultimoalloc[ultimoalloc.Count - 1].ToString());
                            // ultimoalloc.RemoveAt(ultimoalloc.Count - 1);  //comentado saida

                            GeraCodigo.geracodigo.Gera("", "HLT", "", ""); //geracod
                        }
                        else
                        {
                            TrataErroSintatico("Erro: esperado '.'");

                            //MessageBox.Show("Erro: Faltando ponto");
                        }
                    }
                    else
                    {
                        TrataErroSintatico("Erro: esperado ';'");
                        // MessageBox.Show("Erro: Faltando ponto_virgula");
                    }
                }
                else
                {
                    TrataErroSintatico("Erro: esperado 'identificador'");
                    // MessageBox.Show("Erro: Faltando identificador");
                }
            }
            else
            {
                TrataErroSintatico("Erro: esperado 'programa'");

                //MessageBox.Show("Erro: Faltando programa");
            }

        }


        //Geração de Código - INICIO





        //Geração de Código - FIM

        private void TrataErroSintatico(string mensagem)
        {
            if (Semantico.analiseSemantico.terminaProgramaSintatico == 0)
            {
                MessageBox.Show(mensagem, "linha " + token.numLinha);
                Semantico.analiseSemantico.terminaProgramaSintatico = 1;
                //Lexico.analiseLexico.SetTerminaPrograma(terminaProgramaSintatico);

            }
            //dt_erros.Rows.Add(new object[] { token.numLinha, mensagem });

        }


        private void Analisa_bloco(string conteudo)
        {

            token = Lexico.analiseLexico.AnalisadorLexical(conteudo);
            if (Semantico.analiseSemantico.terminaProgramaSintatico == 0)
                Analisa_et_variaveis(conteudo);
            if (Semantico.analiseSemantico.terminaProgramaSintatico == 0)
                Analisa_subrotinas(conteudo);
            if (Semantico.analiseSemantico.terminaProgramaSintatico == 0)
                Analisa_comandos(conteudo);

        }

        private void Analisa_et_variaveis(string conteudo)
        {
            if (string.Equals(token.simbolo, "svar") && Semantico.analiseSemantico.terminaProgramaSintatico == 0)
            {
                totalVar = 0;   //geracod

                token = Lexico.analiseLexico.AnalisadorLexical(conteudo);

                if (string.Equals(token.simbolo, "sidentificador") && Semantico.analiseSemantico.terminaProgramaSintatico == 0)
                {
                    while (string.Equals(token.simbolo, "sidentificador") && Semantico.analiseSemantico.terminaProgramaSintatico == 0)
                    {
                        Analisa_variaveis(conteudo);

                        if (string.Equals(token.simbolo, "sponto_virgula") && Semantico.analiseSemantico.terminaProgramaSintatico == 0)
                        {
                            if (procAlloc == 1)
                            {
                                procAllocCount++;
                            }

                            temAlloc = true;
                            if (temCall)
                            {
                                //espacoMemoria++;
                                temCall = false;
                            }
                            GeraCodigo.geracodigo.Gera("", "ALLOC", espacoMemoria.ToString(), totalVar.ToString()); //geracod
                            Semantico.analiseSemantico.Insere_tabela_alloc(espacoMemoria, totalVar, nivelAlloc);
                            aux_totalvar = totalVar;
                            ultimoalloc.Add(totalVar);
                            espacoMemoria = espacoMemoria + totalVar; //lembrar de fazer "menos" com DALLOC
                            aux_espacoMem = espacoMemoria;
                            totalVar = 0;
                            token = Lexico.analiseLexico.AnalisadorLexical(conteudo);
                        }
                        else
                        {
                            TrataErroSintatico("Erro: esperado ';'");
                            // MessageBox.Show("Erro (Analisa_et_variaveis): Faltando Ponto e Virgula");
                        }
                    }
                }
                else
                {
                    TrataErroSintatico("Erro: esperado 'identificador'");
                    //MessageBox.Show("Erro (Analisa_et_variaveis): Esperado Identificador");
                }

            }
        }


        private void Analisa_variaveis(string conteudo)
        {
            do
            {
                if (string.Equals(token.simbolo, "sidentificador") && Semantico.analiseSemantico.terminaProgramaSintatico == 0)
                {
                    if (!Semantico.analiseSemantico.Pesquisa_duplicvar_tabela(token.lexema, nivelEscopo))
                    {
                        Semantico.analiseSemantico.Insere_tabela(token.lexema, "variavel", nivelEscopo, aux_espacoMem, auxNome_procFunc);
                        aux_espacoMem++;
                        totalVar++;  //geracod
                        token = Lexico.analiseLexico.AnalisadorLexical(conteudo);

                        if (string.Equals(token.simbolo, "svirgula") || string.Equals(token.simbolo, "sdoispontos") && Semantico.analiseSemantico.terminaProgramaSintatico == 0)
                        {
                            if (string.Equals(token.simbolo, "svirgula") && Semantico.analiseSemantico.terminaProgramaSintatico == 0)
                            {
                                token = Lexico.analiseLexico.AnalisadorLexical(conteudo);

                                if (string.Equals(token.simbolo, "sdoispontos") && Semantico.analiseSemantico.terminaProgramaSintatico == 0)
                                {
                                    TrataErroSintatico("Erro: esperado ':' após ','");
                                    //MessageBox.Show("Erro (Analisa_variaveis): Doispontos após a virgula");
                                }
                            }
                        }

                        else
                        {
                            TrataErroSintatico("Erro: esperado ':' ou ','");
                            // MessageBox.Show("Erro (Analisa_variaveis): Esperado doispontos ou virgula");
                        }
                    }
                    else
                    {
                        Semantico.analiseSemantico.TrataErroSemantico("Erro: Variavel duplicada encontrada"); //Inserir o lexema
                    }
                }

                else
                {
                    TrataErroSintatico("Erro: esperado 'identificador'");
                    // MessageBox.Show("Erro (Analisa_variaveis): Esperado identificador");
                }
            } while (!(string.Equals(token.simbolo, "sdoispontos")) && Semantico.analiseSemantico.terminaProgramaSintatico == 0);

            token = Lexico.analiseLexico.AnalisadorLexical(conteudo);
            Analisa_tipo(conteudo);
        }

        private void Analisa_tipo(string conteudo)
        {
            if (!(string.Equals(token.simbolo, "sinteiro")) && !(string.Equals(token.simbolo, "sbooleano")) && Semantico.analiseSemantico.terminaProgramaSintatico == 0)
            {
                TrataErroSintatico("Erro: esperado 'inteiro' ou 'booleano'");
                //MessageBox.Show("Erro (Analisa_tipo): Necessario tipo ser inteiro ou booleano");
            }
            else
            {
                Semantico.analiseSemantico.Coloca_tipo_tabela(token.lexema);
                token = Lexico.analiseLexico.AnalisadorLexical(conteudo);
            }
        }

        private void Analisa_comandos(string conteudo)
        {
            if (string.Equals(token.simbolo, "sinicio") && Semantico.analiseSemantico.terminaProgramaSintatico == 0)
            {
                if (temProcedimento == false)
                {
                    if (nivelEscopo > 0 && auxRot_global > 0)
                    {
                        GeraCodigo.geracodigo.Gera(auxRot_global.ToString(), "NULL", "", ""); //geracod, marca inicio main

                    }
                    else
                    {
                        if (checaFuncao == 0 && checaGera == false)
                        {
                            GeraCodigo.geracodigo.Gera("0", "NULL", "", ""); //geracod, marca inicio main
                        }

                    }

                }
                else
                {

                    //  Gera("0", "NULL", "", ""); //geracod, marca inicio main


                }

                token = Lexico.analiseLexico.AnalisadorLexical(conteudo);
                Analisa_comando_simples(conteudo);

                while (!(string.Equals(token.simbolo, "sfim")) && Semantico.analiseSemantico.terminaProgramaSintatico == 0)
                {
                    if (Semantico.analiseSemantico.Pesquisa_funcao_tabela(token.lexema))
                    {
                        //GeraCodigo.geracodigo.("", "DALLOC", ultimoalloc.ToString(), ""); //geracod
                        token = Lexico.analiseLexico.AnalisadorLexical(conteudo);

                        if (!(string.Equals(token.simbolo, "sfim")) && Semantico.analiseSemantico.terminaProgramaSintatico == 0)
                        {
                            Analisa_comando_simples(conteudo);
                        }

                    }
                    else
                    {
                        if (string.Equals(token.simbolo, "sponto_virgula") && Semantico.analiseSemantico.terminaProgramaSintatico == 0)
                        {
                            //GeraCodigo.geracodigo.("", "DALLOC", ultimoalloc.ToString(), ""); //geracod
                            token = Lexico.analiseLexico.AnalisadorLexical(conteudo);



                            if (!(string.Equals(token.simbolo, "sfim")) && Semantico.analiseSemantico.terminaProgramaSintatico == 0)
                            {
                                Analisa_comando_simples(conteudo);
                            }
                        }
                        else
                        {



                            TrataErroSintatico("Erro: esperado ';'");

                        }
                    }
                }

                if (checaProcedimento.Count > 0)
                {
                    numProcedimentos--;
                    if (numProcedimentos >= 0)
                    {
                        //fazer um if aqui pra fazer todos os returnf dentro do procedimento
                        if (checaFuncao == 1)
                        {
                            //espacoMemoria = espacoMemoria - ultimoalloc[ultimoalloc.Count - 1];



                            Semantico.analiseSemantico.Pesquisa_tabela_alloc(nivelAlloc);
                            nivelAlloc--;



                            if (ultimoProcedimento != "")
                            {

                                bool entrouFor = false;
                                for (int i = 0; i < Semantico.analiseSemantico.aux_tabelaAllocs.Count; i++)
                                {

                                    GeraCodigo.geracodigo.Gera("", "DALLOC", Semantico.analiseSemantico.aux_tabelaAllocs[i].inicio.ToString(), Semantico.analiseSemantico.aux_tabelaAllocs[i].fim.ToString());
                                    entrouFor = true;

                                }

                                if (entrouFor)
                                {
                                    espacoMemoria = espacoMemoria - ultimoalloc[ultimoalloc.Count - 1];
                                    aux_espacoMem = espacoMemoria;
                                    //GeraCodigo.geracodigo.Gera("", "RETURNF", espacoMemoria.ToString(), ultimoalloc[ultimoalloc.Count - 1].ToString());
                                    ultimoalloc.RemoveAt(ultimoalloc.Count - 1);
                                    procAlloc = 0;
                                    Semantico.analiseSemantico.aux_tabelaAllocs.Clear();
                                    temAlloc = false;

                                }
                                checaProcedimento.RemoveAt(0);
                                temProcedimento = false;
                                GeraCodigo.geracodigo.Gera("", "RETURN", "", "");
                                ultimoProcedimento = "";

                            }
                            else
                            {
                                bool entrouFor = false;
                                for (int i = 0; i < Semantico.analiseSemantico.aux_tabelaAllocs.Count; i++)
                                {

                                    GeraCodigo.geracodigo.Gera("", "RETURNF", Semantico.analiseSemantico.aux_tabelaAllocs[i].inicio.ToString(), Semantico.analiseSemantico.aux_tabelaAllocs[i].fim.ToString());
                                    entrouFor = true;


                                }

                                if (entrouFor)
                                {
                                    if (!verificaRetorno)
                                    {

                                        Semantico.analiseSemantico.TrataErroSemantico("ERRO: Funcao sem retorno");
                                    }
                                    checaFuncao = 0;
                                    espacoMemoria = espacoMemoria - ultimoalloc[ultimoalloc.Count - 1];
                                    //GeraCodigo.geracodigo.Gera("", "RETURNF", espacoMemoria.ToString(), ultimoalloc[ultimoalloc.Count - 1].ToString());
                                    ultimoalloc.RemoveAt(ultimoalloc.Count - 1);
                                    procAlloc = 0;
                                    Semantico.analiseSemantico.aux_tabelaAllocs.Clear();
                                    temAlloc = false;


                                }

                            }




                        }
                        else
                        {

                            Semantico.analiseSemantico.Pesquisa_tabela_alloc(nivelAlloc);
                            nivelAlloc--;


                            bool entrouFor = false;
                            for (int i = 0; i < Semantico.analiseSemantico.aux_tabelaAllocs.Count; i++)
                            {

                                GeraCodigo.geracodigo.Gera("", "DALLOC", Semantico.analiseSemantico.aux_tabelaAllocs[i].inicio.ToString(), Semantico.analiseSemantico.aux_tabelaAllocs[i].fim.ToString());
                                entrouFor = true;

                            }



                            // GeraCodigo.geracodigo.Gera("", "DALLOC", espacoMemoria.ToString(), ultimoalloc[ultimoalloc.Count - 1].ToString());
                            // checaProcedimento = 0;
                            checaProcedimento.RemoveAt(0);
                            temProcedimento = false;

                            GeraCodigo.geracodigo.Gera("", "RETURN", "", "");
                            if (entrouFor)
                            {
                                espacoMemoria = espacoMemoria - ultimoalloc[ultimoalloc.Count - 1];
                                aux_espacoMem = espacoMemoria;

                                ultimoalloc.RemoveAt(ultimoalloc.Count - 1);
                                procAlloc = 0;
                                Semantico.analiseSemantico.aux_tabelaAllocs.Clear();
                                temAlloc = false;

                            }



                        }

                    }

                }
                else if (checaFuncao == 1)
                {

                    Semantico.analiseSemantico.Pesquisa_tabela_alloc(nivelAlloc);
                    nivelAlloc--;

                    bool entrouFor = false;
                    for (int i = 0; i < Semantico.analiseSemantico.aux_tabelaAllocs.Count; i++)
                    {

                        GeraCodigo.geracodigo.Gera("", "RETURNF", Semantico.analiseSemantico.aux_tabelaAllocs[i].inicio.ToString(), Semantico.analiseSemantico.aux_tabelaAllocs[i].fim.ToString());

                        checaFuncao = 0;
                        entrouFor = true;


                    }
                    if (entrouFor)
                    {
                        if (!verificaRetorno)
                        {
                            Semantico.analiseSemantico.TrataErroSemantico("ERRO: Funcao sem retorno");
                        }
                        espacoMemoria = espacoMemoria - ultimoalloc[ultimoalloc.Count - 1];
                        //GeraCodigo.geracodigo.Gera("", "RETURNF", espacoMemoria.ToString(), ultimoalloc[ultimoalloc.Count - 1].ToString());
                        ultimoalloc.RemoveAt(ultimoalloc.Count - 1);
                        procAlloc = 0;
                        Semantico.analiseSemantico.aux_tabelaAllocs.Clear();


                    }


                }

                token = Lexico.analiseLexico.AnalisadorLexical(conteudo);
            }
            else
            {
                TrataErroSintatico("Erro: esperado 'inicio'");

                //MessageBox.Show("Erro (Analisa_comandos): Faltando inicio");
            }

        }

        private void Analisa_comando_simples(string conteudo)
        {
            //MessageBox.Show("1 Analisa_comando_simples: " + token.simbolo, token.lexema);

            if (string.Equals(token.simbolo, "sidentificador") && Semantico.analiseSemantico.terminaProgramaSintatico == 0)
            {
                Analisa_atrib_chprocedimento(conteudo);
            }
            else if (string.Equals(token.simbolo, "sse") && Semantico.analiseSemantico.terminaProgramaSintatico == 0)
            {
                //  checaProcedimento = 1;
                checaGera = true;
                Analisa_se(conteudo);
                checaGera = false;
                // checaProcedimento = 0;
            }
            else if (string.Equals(token.simbolo, "senquanto") && Semantico.analiseSemantico.terminaProgramaSintatico == 0)
            {

                checaGera = true;
                Analisa_enquanto(conteudo);
                checaGera = false;

            }
            else if (string.Equals(token.simbolo, "sleia") && Semantico.analiseSemantico.terminaProgramaSintatico == 0)
            {
                checaGera = true;
                Analisa_leia(conteudo);
                checaGera = false;

            }
            else if (string.Equals(token.simbolo, "sescreva") && Semantico.analiseSemantico.terminaProgramaSintatico == 0)
            {
                checaGera = true;
                Analisa_escreva(conteudo);
                checaGera = false;

            }
            else
            {
                if (Semantico.analiseSemantico.terminaProgramaSintatico == 0)
                    Analisa_comandos(conteudo);
            }
        }

        private void Analisa_atrib_chprocedimento(string conteudo)
        {
            string aux;
            aux = token.lexema;
            // string aux_receptor;


            token = Lexico.analiseLexico.AnalisadorLexical(conteudo);
            if (string.Equals(token.simbolo, "satribuicao") && Semantico.analiseSemantico.terminaProgramaSintatico == 0)
            {
                Analisa_atribuicao(conteudo, aux);

                if (temFuncao == false)
                {
                    int posMem;
                    posMem = Semantico.analiseSemantico.Pesquisa_memoria_tabela(aux, nivelEscopo);
                    GeraCodigo.geracodigo.Gera("", "STR", posMem.ToString(), "");
                }
                else
                {
                    temFuncao = false;
                }

            }
            else
            {
                GeraCodigo.geracodigo.Gera("", "CALL", aux, ""); //geracod, usar rotulo no paremetro do CALL?
                                                                 //espacoMemoria++;
                temCall = true;
                //Chamada_procedimento(conteudo) //esse que nao existe? 
            }
        }

        private void Analisa_leia(string conteudo)
        {
            checaInicio++;
            token = Lexico.analiseLexico.AnalisadorLexical(conteudo);
            if (string.Equals(token.simbolo, "sabre_parenteses") && Semantico.analiseSemantico.terminaProgramaSintatico == 0)
            {
                token = Lexico.analiseLexico.AnalisadorLexical(conteudo);
                if (string.Equals(token.simbolo, "sidentificador") && Semantico.analiseSemantico.terminaProgramaSintatico == 0)
                {

                    if (Semantico.analiseSemantico.Pesquisa_declvar_tabela(token.lexema))
                    {
                        GeraCodigo.geracodigo.Gera("", "RD", "", "");
                        //procurar na tabela posicao de memoria do token.lexema

                        int posMem;
                        posMem = Semantico.analiseSemantico.Pesquisa_memoria_tabela(token.lexema, nivelEscopo);
                        GeraCodigo.geracodigo.Gera("", "STR", posMem.ToString(), "");


                        token = Lexico.analiseLexico.AnalisadorLexical(conteudo);

                        if (string.Equals(token.simbolo, "sfecha_parenteses") && Semantico.analiseSemantico.terminaProgramaSintatico == 0)
                        {
                            token = Lexico.analiseLexico.AnalisadorLexical(conteudo);
                        }
                        else
                        {
                            TrataErroSintatico("Erro: esperado ')'");

                            //MessageBox.Show("Erro (Analisa_leia): Esperado fecha_parenteses");
                        }
                    }
                    else
                    {
                        Semantico.analiseSemantico.TrataErroSemantico("Erro: Variavel não declarada");
                    }
                }
                else
                {
                    TrataErroSintatico("Erro: esperado 'identificador'");

                    //MessageBox.Show("Erro (Analisa_leia): Esperado identificador");
                }
            }
            else
            {
                TrataErroSintatico("Erro: esperado '('");
                // MessageBox.Show("Erro(Analisa_leia): Esperado abre_parenteses");
            }
        }


        private void Analisa_escreva(string conteudo)
        {
            bool ret;
            checaInicio++;
            token = Lexico.analiseLexico.AnalisadorLexical(conteudo);

            if (string.Equals(token.simbolo, "sabre_parenteses") && Semantico.analiseSemantico.terminaProgramaSintatico == 0)
            {
                token = Lexico.analiseLexico.AnalisadorLexical(conteudo);
                if (string.Equals(token.simbolo, "sidentificador") && Semantico.analiseSemantico.terminaProgramaSintatico == 0)
                {
                    ret = Semantico.analiseSemantico.Pesquisa_funcao_tabela(token.lexema);

                    if (ret == false)
                    {
                        //procurar na tabela posicao de memoria do token.lexema
                        int posMem;
                        posMem = Semantico.analiseSemantico.Pesquisa_memoria_tabela(token.lexema, nivelEscopo);
                        GeraCodigo.geracodigo.Gera("", "LDV", posMem.ToString(), "");

                    }
                    else
                    {
                        GeraCodigo.geracodigo.Gera("", "CALL", token.lexema, "");
                        //espacoMemoria++;

                    }

                    GeraCodigo.geracodigo.Gera("", "PRN", "", "");


                    token = Lexico.analiseLexico.AnalisadorLexical(conteudo);
                    if (string.Equals(token.simbolo, "sfecha_parenteses") && Semantico.analiseSemantico.terminaProgramaSintatico == 0)
                    {
                        token = Lexico.analiseLexico.AnalisadorLexical(conteudo);
                    }
                    else
                    {
                        TrataErroSintatico("Erro: esperado ')'");
                        //MessageBox.Show("Erro (Analisa_escreva): Esperado fecha_parenteses");
                    }
                }
                else
                {
                    TrataErroSintatico("Erro: esperado 'identificador'");

                    // MessageBox.Show("Erro (Analisa_escreva): Esperado identificador");
                }
            }
            else
            {
                TrataErroSintatico("Erro: esperado '('");

                //MessageBox.Show("Erro (Analisa_escreva): Esperado abre_parenteses");
            }

        }

        private void Analisa_enquanto(string conteudo)
        {
            checaInicio++;
            int auxrot1, auxrot2;          //geracod
            auxrot1 = rotulo;           //geracod
            GeraCodigo.geracodigo.Gera(rotulo.ToString(), "NULL", "", ""); //geracod
            rotulo++;   //geracod
            string infix = "";
            auxExpressao.Clear();
            token = Lexico.analiseLexico.AnalisadorLexical(conteudo);
            Analisa_expressao(conteudo);

            GeraCodigo.geracodigo.VerificaUnario();
            for (int index = 0; index < auxExpressao.Count(); index++)
            {
                //MessageBox.Show(auxExpressao[index]);
                infix = infix + auxExpressao[index];
            }

            GeraCodigo.geracodigo.convertePost(ref auxExpressao);


            if (!Semantico.analiseSemantico.Verifica_booleano())
            {
                Semantico.analiseSemantico.TrataErroSemantico("ERRO: Expressao nao booleana");
            }


            if (string.Equals(token.simbolo, "sfaca") && Semantico.analiseSemantico.terminaProgramaSintatico == 0)
            {
                auxrot2 = rotulo; //geracod
                GeraCodigo.geracodigo.Gera("", "JMPF", rotulo.ToString(), ""); //geracod
                rotulo++; //geracod

                token = Lexico.analiseLexico.AnalisadorLexical(conteudo);

                Analisa_comando_simples(conteudo);
                GeraCodigo.geracodigo.Gera("", "JMP", auxrot1.ToString(), ""); //geracod
                GeraCodigo.geracodigo.Gera(auxrot2.ToString(), "NULL", "", ""); //geracod
            }
            else
            {
                TrataErroSintatico("Erro: esperado 'faca'");

                //  MessageBox.Show("Erro (Analisa_enquanto): Esperado faça");
            }
        }




        private void Analisa_se(string conteudo)
        {
            checaInicio++;
            string infix = "";
            int temSenao = 0;
            auxExpressao.Clear();
            token = Lexico.analiseLexico.AnalisadorLexical(conteudo);
            Analisa_expressao(conteudo);
            int aux_rot1;

            GeraCodigo.geracodigo.VerificaUnario();

            for (int index = 0; index < auxExpressao.Count(); index++)
            {
                //MessageBox.Show(auxExpressao[index]);
                infix = infix + auxExpressao[index];
            }

            GeraCodigo.geracodigo.convertePost(ref auxExpressao);

            if (!Semantico.analiseSemantico.Verifica_booleano())
            {
                Semantico.analiseSemantico.TrataErroSemantico("ERRO: Expressao nao booleana");
            }

            //token = Lexico.analiseLexico.AnalisadorLexical(conteudo);
            //token = Lexico.analiseLexico.AnalisadorLexical(conteudo);
            //token = Lexico.analiseLexico.AnalisadorLexical(conteudo);
            //MessageBox.Show("Analisa_se: " + token.simbolo, token.lexema);

            if (string.Equals(token.simbolo, "sentao") && Semantico.analiseSemantico.terminaProgramaSintatico == 0)
            {
                rotulo++;
                aux_rot1 = rotulo;
                GeraCodigo.geracodigo.Gera("", "JMPF", rotulo.ToString(), ""); //geracod
                token = Lexico.analiseLexico.AnalisadorLexical(conteudo);
                Analisa_comando_simples(conteudo);



                if (string.Equals(token.simbolo, "ssenao"))
                {

                    rotulo++;
                    GeraCodigo.geracodigo.Gera("", "JMP", rotulo.ToString(), ""); //geracod

                    temSenao = 1;

                    GeraCodigo.geracodigo.Gera(aux_rot1.ToString(), "NULL", "", ""); //geracod


                    token = Lexico.analiseLexico.AnalisadorLexical(conteudo);
                    Analisa_comando_simples(conteudo);

                    GeraCodigo.geracodigo.Gera(rotulo.ToString(), "NULL", "", ""); //geracod
                    rotulo++;

                    //rotulo = aux_rot1;
                }
            }
            else
            {
                TrataErroSintatico("Erro: esperado 'entao'");

                // MessageBox.Show("Erro (Analisa_se): Esperado um entao");
            }

            if (temSenao == 0)
            {
                GeraCodigo.geracodigo.Gera(rotulo.ToString(), "NULL", "", ""); //geracod
                rotulo++;
            }


        }

        private void Analisa_subrotinas(string conteudo)
        {
            /*int auxrot, flagSubRotina; //geracod
            //flagSubRotina = 1;  //geracod
            //auxrot = rotulo; //geracod
            if (string.Equals(token.simbolo, "sprocedimento") || string.Equals(token.simbolo, "sfuncao") && terminaProgramaSintatico == 0)
            {
                auxrot = rotulo; //geracod
                GeraCodigo.geracodigo.Gera("", "JMP", rotulo.ToString(), "");
                rotulo++;
                flagSubRotina = 1;
            }*/



            while (string.Equals(token.simbolo, "sprocedimento") || string.Equals(token.simbolo, "sfuncao") && Semantico.analiseSemantico.terminaProgramaSintatico == 0)
            {


                if (string.Equals(token.simbolo, "sprocedimento") && Semantico.analiseSemantico.terminaProgramaSintatico == 0)
                {
                    ultimoProcedimento = token.lexema;
                    procAlloc = 1;
                    numProcedimentos++;
                    nivelAlloc++;
                    Analisa_declaracao_procedimento(conteudo);
                }
                else
                {
                    nivelAlloc++;
                    checaFuncao = 1;
                    Analisa_declaracao_funcao(conteudo);
                }

                //token = Lexico.analiseLexico.AnalisadorLexical(conteudo);
                if (string.Equals(token.simbolo, "sponto_virgula") && Semantico.analiseSemantico.terminaProgramaSintatico == 0)
                {
                    token = Lexico.analiseLexico.AnalisadorLexical(conteudo);
                }
                else
                {
                    TrataErroSintatico("Erro: esperado ';'");

                    // MessageBox.Show("Erro (Analisa_subrotinas): Esperado um ponto_virgula ", token.simbolo);
                }
            }

            /*if(flagSubRotina == 1)
            {
                GeraCodigo.geracodigo.Gera(auxrot.ToString(), "NULL", "", "");
            }*/


        }

        private void Analisa_declaracao_procedimento(string conteudo)
        {
            nivelEscopo++;
            token = Lexico.analiseLexico.AnalisadorLexical(conteudo);
            if (string.Equals(token.simbolo, "sidentificador") && Semantico.analiseSemantico.terminaProgramaSintatico == 0)
            {
                if (!Semantico.analiseSemantico.Pesquisa_declproc_tabela(token.lexema))
                {
                    if (nivelEscopo > 1)
                    {
                        auxNome_procFunc = token.lexema;
                        Semantico.analiseSemantico.Insere_tabela(token.lexema, "procedimento", nivelEscopo, espacoMemoria, auxNome_procFunc);
                        if (nivelIgual)
                        {
                            rotulo++;//geracod
                            nivelIgual = false;
                        }


                        auxRot_global = rotulo;
                        GeraCodigo.geracodigo.Gera("", "JMP", rotulo.ToString(), ""); //geracod VAI PRO INICIO DO PROGRAMA

                        GeraCodigo.geracodigo.Gera(token.lexema, "NULL", "", ""); //geracod
                        checaProcedimento.Add(1);
                        temProcedimento = true;
                        //rotulo--;//geracod
                    }
                    else
                    {
                        auxNome_procFunc = token.lexema;
                        Semantico.analiseSemantico.Insere_tabela(token.lexema, "procedimento", nivelEscopo, espacoMemoria, auxNome_procFunc);
                        GeraCodigo.geracodigo.Gera("", "JMP", "0", ""); //geracod VAI PRO INICIO DO PROGRAMA
                        GeraCodigo.geracodigo.Gera(token.lexema, "NULL", "", ""); //geracod
                        checaProcedimento.Add(1);

                        temProcedimento = true;
                        rotulo++;//geracod
                    }




                    token = Lexico.analiseLexico.AnalisadorLexical(conteudo);
                    if (string.Equals(token.simbolo, "sponto_virgula") && Semantico.analiseSemantico.terminaProgramaSintatico == 0)
                    {
                        Analisa_bloco(conteudo);
                    }
                    else
                    {
                        TrataErroSintatico("Erro: esperado ';'");

                        //MessageBox.Show("Erro (Analisa_declaracao_procedimento): Esperado um ponto_virgula");
                    }
                }
                else
                {
                    Semantico.analiseSemantico.TrataErroSemantico("ERRO: Procedimento já declarado");
                }

            }
            else
            {
                TrataErroSintatico("Erro: esperado 'identificador'");

                //MessageBox.Show("Erro (Analisa_declaracao_procedimento): Esperado um identificador");
            }
            nivelEscopo--;

        }

        private void Analisa_declaracao_funcao(string conteudo)
        {
            verificaRetorno = false;
            nivelEscopo++;
            token = Lexico.analiseLexico.AnalisadorLexical(conteudo);
            if (string.Equals(token.simbolo, "sidentificador") && Semantico.analiseSemantico.terminaProgramaSintatico == 0)
            {
                if (!Semantico.analiseSemantico.Pesquisa_declfuncao_tabela(token.lexema))
                {
                    if (nivelEscopo > 1)
                    {
                        auxNome_procFunc = token.lexema;
                        Semantico.analiseSemantico.Insere_tabela(token.lexema, "", nivelEscopo, espacoMemoria, auxNome_procFunc);
                        if (nivelIgual)
                        {
                            rotulo++;//geracod
                            nivelIgual = false;
                        }
                        auxRot_global = rotulo;
                        GeraCodigo.geracodigo.Gera("", "JMP", rotulo.ToString(), ""); //geracod VAI PRO INICIO DO PROGRAMA
                        GeraCodigo.geracodigo.Gera(token.lexema, "NULL", "", ""); //geracod
                                                                                  // checaProcedimento = 1;
                                                                                  // rotulo--;//geracod
                    }
                    else
                    {
                        auxNome_procFunc = token.lexema;
                        Semantico.analiseSemantico.Insere_tabela(token.lexema, "", nivelEscopo, espacoMemoria, auxNome_procFunc);
                        GeraCodigo.geracodigo.Gera("", "JMP", "0", ""); //geracod VAI PRO INICIO DO PROGRAMA
                        GeraCodigo.geracodigo.Gera(token.lexema, "NULL", "", ""); //geracod
                                                                                  // checaProcedimento = 1;
                        rotulo++;//geracod
                    }




                    token = Lexico.analiseLexico.AnalisadorLexical(conteudo);
                    if (string.Equals(token.simbolo, "sdoispontos") && Semantico.analiseSemantico.terminaProgramaSintatico == 0)
                    {

                        token = Lexico.analiseLexico.AnalisadorLexical(conteudo);
                        if (string.Equals(token.simbolo, "sinteiro") || string.Equals(token.simbolo, "sbooleano") && Semantico.analiseSemantico.terminaProgramaSintatico == 0)
                        {
                            if (token.simbolo == "sinteiro")
                            {
                                Semantico.analiseSemantico.tabelaSimbolos[Semantico.analiseSemantico.tabelaSimbolos.Count - 1].tipo = "funcao inteiro";
                            }
                            else
                            {
                                Semantico.analiseSemantico.tabelaSimbolos[Semantico.analiseSemantico.tabelaSimbolos.Count - 1].tipo = "funcao booleano";

                            }
                            token = Lexico.analiseLexico.AnalisadorLexical(conteudo);
                            if (string.Equals(token.simbolo, "sponto_virgula"))
                            {
                                Analisa_bloco(conteudo);
                            }
                        }
                        else
                        {
                            TrataErroSintatico("Erro: esperado 'inteiro' ou 'booleano'");

                            //MessageBox.Show("Erro (Analisa_declaracao_funcao): Esperado um inteiro ou booleano");
                        }
                    }
                    else
                    {
                        TrataErroSintatico("Erro: esperado ':'");

                        //MessageBox.Show("Erro (Analisa_declaracao_funcao): Esperado um doispontos");
                    }
                }
                else
                {
                    Semantico.analiseSemantico.TrataErroSemantico("ERRO: Funcao ja declarada");
                }
            }
            else
            {
                TrataErroSintatico("Erro: esperado 'identificador'");

                // MessageBox.Show("Erro (Analisa_declaracao_funcao): Esperado um identificador");
            }
            nivelEscopo--;
        }

        private void Analisa_expressao(string conteudo)
        {
            //auxExpressao.Add(token.lexema);
            Analisa_expressao_simples(conteudo);

            if (string.Equals(token.simbolo, "smaior") || string.Equals(token.simbolo, "smaiorig") || string.Equals(token.simbolo, "sig") || string.Equals(token.simbolo, "smenor") || string.Equals(token.simbolo, "smenorig") || string.Equals(token.simbolo, "sdif") && Semantico.analiseSemantico.terminaProgramaSintatico == 0)
            {
                auxExpressao.Add(token.lexema);
                token = Lexico.analiseLexico.AnalisadorLexical(conteudo);
                Analisa_expressao_simples(conteudo);
            }
        }

        private void Analisa_expressao_simples(string conteudo) //cuidado
        {
            if (Semantico.analiseSemantico.terminaProgramaSintatico == 0)
            {


                if (string.Equals(token.simbolo, "smais") || string.Equals(token.simbolo, "smenos") && Semantico.analiseSemantico.terminaProgramaSintatico == 0)
                {
                    auxExpressao.Add(token.lexema);
                    token = Lexico.analiseLexico.AnalisadorLexical(conteudo);
                }
                Analisa_termo(conteudo);
                while (string.Equals(token.simbolo, "smais") || string.Equals(token.simbolo, "smenos") || string.Equals(token.simbolo, "sou") && Semantico.analiseSemantico.terminaProgramaSintatico == 0)
                {
                    auxExpressao.Add(token.lexema);
                    token = Lexico.analiseLexico.AnalisadorLexical(conteudo);
                    Analisa_termo(conteudo);
                }
            }
        }

        private void Analisa_termo(string conteudo)
        {
            if (Semantico.analiseSemantico.terminaProgramaSintatico == 0)
            {
                Analisa_fator(conteudo);
                while (string.Equals(token.simbolo, "smult") || string.Equals(token.simbolo, "sdiv") || string.Equals(token.simbolo, "se") && Semantico.analiseSemantico.terminaProgramaSintatico == 0)
                {
                    auxExpressao.Add(token.lexema);
                    token = Lexico.analiseLexico.AnalisadorLexical(conteudo);
                    Analisa_fator(conteudo);
                }
            }
        }

        private void Analisa_fator(string conteudo)
        {
            //MessageBox.Show("1 Analisa_fator: " + token.simbolo, token.lexema);
            if (Semantico.analiseSemantico.terminaProgramaSintatico == 0)
            {
                if (string.Equals(token.simbolo, "sidentificador") && Semantico.analiseSemantico.terminaProgramaSintatico == 0)
                {




                    int ret;
                    ret = Semantico.analiseSemantico.Pesquisa_tabela(token.lexema, nivelEscopo);
                    if (ret != -1)
                    {
                        if (Semantico.analiseSemantico.tabelaSimbolos[ret].tipo == "funcao inteiro" || Semantico.analiseSemantico.tabelaSimbolos[ret].tipo == "funcao booleano")
                        {
                            auxExpressao.Add(token.lexema);
                            GeraCodigo.geracodigo.Gera("", "CALL", token.lexema, "");
                            //  espacoMemoria++;
                            temCall = true;
                            //Analisa_chamada_função();  o que fará?? semantico
                        }
                        else
                        {
                            auxExpressao.Add(token.lexema);
                            token = Lexico.analiseLexico.AnalisadorLexical(conteudo); //!!!!  
                        }
                    }
                    else
                    {
                        MessageBox.Show(token.lexema);
                        Semantico.analiseSemantico.TrataErroSemantico("Identificador não declarado");

                    }

                }
                else if (string.Equals(token.simbolo, "snumero") && Semantico.analiseSemantico.terminaProgramaSintatico == 0)
                {
                    auxExpressao.Add(token.lexema);
                    token = Lexico.analiseLexico.AnalisadorLexical(conteudo);
                }
                else if (string.Equals(token.simbolo, "snao") && Semantico.analiseSemantico.terminaProgramaSintatico == 0)
                {
                    auxExpressao.Add(token.lexema);
                    token = Lexico.analiseLexico.AnalisadorLexical(conteudo);
                    Analisa_fator(conteudo);
                }
                else if (string.Equals(token.simbolo, "sabre_parenteses") && Semantico.analiseSemantico.terminaProgramaSintatico == 0)
                {
                    auxExpressao.Add(token.lexema);
                    token = Lexico.analiseLexico.AnalisadorLexical(conteudo);
                    Analisa_expressao(conteudo);
                    // token = Lexico.analiseLexico.AnalisadorLexical(conteudo);
                    // token = Lexico.analiseLexico.AnalisadorLexical(conteudo);
                    // token = Lexico.analiseLexico.AnalisadorLexical(conteudo);

                    if (string.Equals(token.simbolo, "sfecha_parenteses") && Semantico.analiseSemantico.terminaProgramaSintatico == 0)
                    {
                        auxExpressao.Add(token.lexema);
                        token = Lexico.analiseLexico.AnalisadorLexical(conteudo);
                    }
                    else
                    {
                        TrataErroSintatico("Erro: esperado ')'");

                        //  MessageBox.Show("Erro (Analisa_fator): Esperado fecha_parenteses");
                    }
                }
                else if (string.Equals(token.lexema, "verdadeiro") || string.Equals(token.lexema, "falso") && Semantico.analiseSemantico.terminaProgramaSintatico == 0)
                {
                    auxExpressao.Add(token.lexema);
                    auxNome_procFunc = token.lexema;
                    Semantico.analiseSemantico.Insere_tabela(token.lexema, "booleano", nivelEscopo, -1, auxNome_procFunc);
                    token = Lexico.analiseLexico.AnalisadorLexical(conteudo);
                }
                else
                {
                    TrataErroSintatico("Erro: operador lógico extra");

                    //MessageBox.Show("Erro (Analisa_fator): Possivel erro com operador lógico extra");
                }
            }
        }

        private void Analisa_atribuicao(string conteudo, string aux)
        {
            string infix = "";
            auxExpressao.Clear();
            if (Semantico.analiseSemantico.terminaProgramaSintatico == 0)
            {
                token = Lexico.analiseLexico.AnalisadorLexical(conteudo);
                Analisa_expressao(conteudo);


                GeraCodigo.geracodigo.VerificaUnario();

                for (int index = 0; index < auxExpressao.Count(); index++)
                {
                    //MessageBox.Show(auxExpressao[index]);
                    infix = infix + auxExpressao[index];
                }

                GeraCodigo.geracodigo.convertePost(ref auxExpressao);


                Semantico.analiseSemantico.Insere_verificaTipo();

                if (!Semantico.analiseSemantico.Verifica_compatibilidade(aux))
                {
                    Semantico.analiseSemantico.TrataErroSemantico("ERRO: Incompatibilidade de tipos na atribuição");
                }
            }
        }
    }
}
