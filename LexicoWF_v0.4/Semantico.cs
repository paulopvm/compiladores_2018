﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using SintaticoWF;
using GeraCodigoWF;
using LexicoWF;
using SimboloWF;
namespace SemanticoWF
{
    public class Semantico

    {
        public static Semantico analiseSemantico = new Semantico();

        public List<Simbolo> tabelaSimbolos = new List<Simbolo>(); //semantico
        public List<Alloc> tabelaAllocs = new List<Alloc>(); //semantico
        public List<Alloc> aux_tabelaAllocs = new List<Alloc>(); //semantico
        public Sintatico sintatico = new Sintatico();
        public List<Simbolo> verificaTipo = new List<Simbolo>();
        public int terminaProgramaSintatico = 0;
        public Token token;
        //Metodos Semanticos - INICIO



        public void Insere_tabela(string slexema, string stipo, int snivel, int smemoria, string snomeaux)
        {

            tabelaSimbolos.Add(new Simbolo() { lexema = slexema, tipo = stipo, nivel = snivel, memoria = smemoria, nomeaux = snomeaux });

            System.Console.WriteLine("Lexema {0}, Memoria {1}\n", tabelaSimbolos[tabelaSimbolos.Count - 1].lexema, tabelaSimbolos[tabelaSimbolos.Count - 1].memoria);

            //System.Console.WriteLine("Lexema {0}, Memoria {1}, Nivel {2}, Tipo {3} \n", tabelaSimbolos[tabelaSimbolos.Count - 1].lexema,tabelaSimbolos[tabelaSimbolos.Count - 1].memoria,tabelaSimbolos[tabelaSimbolos.Count - 1].nivel,tabelaSimbolos[tabelaSimbolos.Count - 1].tipo);
        }

        public void Insere_tabela_alloc(int sinicio, int sfim, int snivel)
        {
            tabelaAllocs.Add(new Alloc() { inicio = sinicio, fim = sfim, nivel = snivel });

            //System.Console.WriteLine("Lexema {0}, Memoria {1}, Nivel {2}, Tipo {3} \n", tabelaSimbolos[tabelaSimbolos.Count - 1].lexema,tabelaSimbolos[tabelaSimbolos.Count - 1].memoria,tabelaSimbolos[tabelaSimbolos.Count - 1].nivel,tabelaSimbolos[tabelaSimbolos.Count - 1].tipo);
        }

        public bool Pesquisa_duplicvar_tabela(string slexema, int snivel)
        {
            if (PesquisaLocal(slexema, snivel))
            {
                return true; //duplicado
            }
            else
            {
                return false;
            }
        }

        public bool PesquisaLocal(string slexema, int snivel)
        {
            bool duplicada = false;
            for (int i = tabelaSimbolos.Count - 1; i > 0; i--)
            {

                if ((tabelaSimbolos[i].lexema == slexema && tabelaSimbolos[i].nivel == snivel))
                {
                    duplicada = true; //duplicado
                    break;
                }
                else if (tabelaSimbolos[i].nivel == snivel)
                {
                    duplicada = false;
                }
            }
            return duplicada;
        }

        public void TrataErroSemantico(string mensagem)
        {
            if (terminaProgramaSintatico == 0)
            {
                MessageBox.Show(mensagem, "linha " + token.numLinha);
                terminaProgramaSintatico = 1;
                //analiseLex.SetTerminaPrograma(terminaProgramaSintatico);

            }
        }

        public void Coloca_tipo_tabela(string slexema)
        {
            //Procurar na tabela de simbolos variaveis com o "tipo variavel" e substituir pelo seu tipo

            for (int i = tabelaSimbolos.Count - 1; i > 0; i--)
            {
                if (tabelaSimbolos[i].tipo == "variavel")
                {
                    if (slexema == "ooleano")
                    {
                        slexema = "booleano";
                    }

                    if (slexema == "nteiro")
                    {
                        slexema = "inteiro";
                    }
                    tabelaSimbolos[i].tipo = slexema;

                }
                //System.Console.WriteLine("Apos Colocar tipo: Lexema {0}, Memoria {1}, Nivel {2}, Tipo {3} \n", tabelaSimbolos[i].lexema, tabelaSimbolos[i].memoria, tabelaSimbolos[i].nivel, tabelaSimbolos[i].tipo);

            }

        }

        public bool Pesquisa_declvar_tabela(string slexema)
        {
            //Pesquisa elementos que possuam tipo inteiro ou booleano, e compara o lexema desse tipo com o lexema a se comparar
            for (int i = tabelaSimbolos.Count - 1; i > 0; i--)
            {

                if (tabelaSimbolos[i].lexema == slexema)
                {
                    if ((tabelaSimbolos[i].tipo == "inteiro" || tabelaSimbolos[i].tipo == "booleano"))
                    {
                        return true;

                    }
                    System.Console.WriteLine("Pesquisa declaracao de var na tabela: Lexema {0}, Memoria {1}, Nivel {2}, Tipo {3} \n", tabelaSimbolos[i].lexema, tabelaSimbolos[i].memoria, tabelaSimbolos[i].nivel, tabelaSimbolos[i].tipo);
                }





            }
            return false;
        }

        /* public bool Pesquisa_declvarfunc_tabela(string slexema)
         {
             //Pesquisa elementos que possuam tipo inteiro ou booleano, e compara o lexema desse tipo com o lexema a se comparar
             for (int i = tabelaSimbolos.Count - 1; i > 0; i--)
             {

                 if (tabelaSimbolos[i].lexema == slexema)
                 {
                     if ((tabelaSimbolos[i].tipo == "inteiro" || tabelaSimbolos[i].tipo == "booleano"))
                     {
                         return true;

                     }
                     System.Console.WriteLine("Pesquisa declaracao de var na tabela: Lexema {0}, Memoria {1}, Nivel {2}, Tipo {3} \n", tabelaSimbolos[i].lexema, tabelaSimbolos[i].memoria, tabelaSimbolos[i].nivel, tabelaSimbolos[i].tipo);
                 }





             }
             return false;
         }*/

        public int Pesquisa_tabela(string slexema, int snivel)
        {
            for (int i = tabelaSimbolos.Count - 1; i > 0; i--)
            {
                if (snivel > 1)
                {

                    if (tabelaSimbolos[i].lexema == slexema && (tabelaSimbolos[i].nivel == snivel || tabelaSimbolos[i].nivel == snivel - 1 || tabelaSimbolos[i].tipo == "funcao inteiro" || tabelaSimbolos[i].tipo == "funcao booleano"))
                    {
                        System.Console.WriteLine("Lexema{0} Memoria {1}", tabelaSimbolos[i].lexema, tabelaSimbolos[i].memoria);
                        return i;

                    }
                    else if (tabelaSimbolos[i].lexema == slexema && (tabelaSimbolos[i].nivel == 0 || tabelaSimbolos[i].tipo == "funcao inteiro" || tabelaSimbolos[i].tipo == "funcao booleano"))
                    {
                        System.Console.WriteLine("Lexema{0} Memoria {1}", tabelaSimbolos[i].lexema, tabelaSimbolos[i].memoria);
                        return i;
                    }
                }
                else
                {
                    if (tabelaSimbolos[i].lexema == slexema && (tabelaSimbolos[i].nivel == snivel || tabelaSimbolos[i].tipo == "funcao inteiro" || tabelaSimbolos[i].tipo == "funcao booleano"))
                    {
                        System.Console.WriteLine("Lexema{0} Memoria {1}", tabelaSimbolos[i].lexema, tabelaSimbolos[i].memoria);
                        return i;

                    }
                    else if (tabelaSimbolos[i].lexema == slexema && (tabelaSimbolos[i].nivel == 0 || tabelaSimbolos[i].tipo == "funcao inteiro" || tabelaSimbolos[i].tipo == "funcao booleano"))
                    {
                        System.Console.WriteLine("Lexema{0} Memoria {1}", tabelaSimbolos[i].lexema, tabelaSimbolos[i].memoria);
                        return i;
                    }

                }



                //System.Console.WriteLine("Apos Colocar tipo: Lexema {0}, Memoria {1}, Nivel {2}, Tipo {3} \n", tabelaSimbolos[i].lexema, tabelaSimbolos[i].memoria, tabelaSimbolos[i].nivel, tabelaSimbolos[i].tipo);

            }
            return -1;
        }

        public void Pesquisa_tabela_alloc(int snivel)
        {
            for (int i = tabelaAllocs.Count - 1; i >= 0; i--)
            {
                if (tabelaAllocs[i].nivel == snivel)
                {
                    aux_tabelaAllocs.Add(new Alloc() { inicio = tabelaAllocs[i].inicio, fim = tabelaAllocs[i].fim, nivel = 0 });

                    tabelaAllocs.RemoveAt(i);
                }

            }
        }

        public bool Pesquisa_funcao_tabela(string slexema)
        {
            for (int i = tabelaSimbolos.Count - 1; i > 0; i--)
            {
                if (tabelaSimbolos[i].lexema == slexema && (tabelaSimbolos[i].tipo == "funcao inteiro" || tabelaSimbolos[i].tipo == "funcao booleano"))
                {
                    System.Console.WriteLine("Lexema{0} Memoria {1}", tabelaSimbolos[i].lexema, tabelaSimbolos[i].memoria);
                    return true;

                }

            }
            return false;
        }

        public int Pesquisa_memoria_tabela(string slexema, int snivel) // verificar quando tem funcao dentro de funcao
        {

            for (int i = tabelaSimbolos.Count - 1; i > 0; i--)
            {
                if (snivel > 1)
                {
                    if (tabelaSimbolos[i].lexema == slexema && (tabelaSimbolos[i].nivel == snivel || tabelaSimbolos[i].nivel == snivel - 1))
                    {
                        System.Console.WriteLine("Local Lexema{0} Memoria {1}", tabelaSimbolos[i].lexema, tabelaSimbolos[i].memoria);
                        return tabelaSimbolos[i].memoria;

                    }
                    else if (tabelaSimbolos[i].lexema == slexema && tabelaSimbolos[i].nivel == 0)
                    {
                        System.Console.WriteLine("Global Lexema{0} Memoria {1}", tabelaSimbolos[i].lexema, tabelaSimbolos[i].memoria);
                        return tabelaSimbolos[i].memoria;
                    }
                }
                else
                {
                    if (tabelaSimbolos[i].lexema == slexema && tabelaSimbolos[i].nivel == snivel)
                    {
                        System.Console.WriteLine("Local Lexema{0} Memoria {1}", tabelaSimbolos[i].lexema, tabelaSimbolos[i].memoria);
                        return tabelaSimbolos[i].memoria;

                    }
                    else if (tabelaSimbolos[i].lexema == slexema && tabelaSimbolos[i].nivel == 0)
                    {
                        System.Console.WriteLine("Global Lexema{0} Memoria {1}", tabelaSimbolos[i].lexema, tabelaSimbolos[i].memoria);
                        return tabelaSimbolos[i].memoria;
                    }

                }

            }


            return -1;
        }

        public bool Pesquisa_declfuncao_tabela(string slexema)
        {
            for (int i = tabelaSimbolos.Count - 1; i > 0; i--)
            {
                if (tabelaSimbolos[i].lexema == slexema && tabelaSimbolos[i].tipo == "funcao")
                {
                    return true;
                }

            }

            return false;
        }

        public bool Pesquisa_declproc_tabela(string slexema)
        {
            for (int i = tabelaSimbolos.Count - 1; i > 0; i--)
            {
                if (tabelaSimbolos[i].lexema == slexema && tabelaSimbolos[i].tipo == "procedimento")
                {
                    return true;
                }

            }

            return false;
        }


        //Metodos Semanticos - FIM


        public void Insere_verificaTipo()
        {


            for (int i = 0; i < sintatico.auxExpressao.Count; i++)
            {
                if (sintatico.auxExpressao[i] != "!=" && sintatico.auxExpressao[i] != "e" && sintatico.auxExpressao[i] != "ou" && sintatico.auxExpressao[i] != ">" && sintatico.auxExpressao[i] != "<" && sintatico.auxExpressao[i] != ">=" && sintatico.auxExpressao[i] != "<=" && sintatico.auxExpressao[i] != "-" && sintatico.auxExpressao[i] != "+" && sintatico.auxExpressao[i] != "*" && sintatico.auxExpressao[i] != "div")
                {
                    if (int.TryParse(sintatico.auxExpressao[i], out int s))
                    {
                        verificaTipo.Add(new Simbolo() { lexema = sintatico.auxExpressao[i], tipo = "inteiro", nivel = -1, memoria = -1 });

                    }
                    else
                    {
                        int ret;
                        ret = Pesquisa_tabela(sintatico.auxExpressao[i], sintatico.nivelEscopo);

                        if (ret != -1)
                        {
                            if (tabelaSimbolos[ret].tipo == "inteiro" || tabelaSimbolos[ret].tipo == "booleano" || tabelaSimbolos[ret].tipo == "funcao inteiro" || tabelaSimbolos[ret].tipo == "funcao booleano")
                            {
                                verificaTipo.Add(new Simbolo() { lexema = tabelaSimbolos[ret].lexema, tipo = tabelaSimbolos[ret].tipo, nivel = -1, memoria = -1 });

                            }
                        }

                    }
                }

            }




        }

        public bool Verifica_compatibilidade(string slexema) //remover da tabela depois de verificar compatibilidade
        {
            int ret;

            bool compativel = false;
            ret = Pesquisa_tabela(slexema, sintatico.nivelEscopo);
            if (ret != -1)
            {
                for (int i = 0; i < verificaTipo.Count; i++)
                {
                    if (tabelaSimbolos[ret].tipo == verificaTipo[i].tipo || ((int.TryParse(verificaTipo[i].lexema, out int g)) && (tabelaSimbolos[ret].tipo == "inteiro")) || (tabelaSimbolos[ret].tipo == "inteiro" && verificaTipo[i].tipo == "funcao inteiro") || (tabelaSimbolos[ret].tipo == "funcao inteiro" && verificaTipo[i].tipo == "inteiro") || (tabelaSimbolos[ret].tipo == "booleano" && verificaTipo[i].tipo == "funcao booleano") || (tabelaSimbolos[ret].tipo == "funcao booleano" && verificaTipo[i].tipo == "booleano"))
                    {
                        if (tabelaSimbolos[ret].tipo == "funcao inteiro" || tabelaSimbolos[ret].tipo == "funcao booleano")
                        {
                            sintatico.verificaRetorno = true;
                        }

                        if ((tabelaSimbolos[ret].tipo == "inteiro" && verificaTipo[i].tipo == "funcao inteiro") || (tabelaSimbolos[ret].tipo == "funcao inteiro" && verificaTipo[i].tipo == "inteiro"))
                        {
                            sintatico.temFuncao = true;
                        }
                        compativel = true;
                    }
                    else
                    {
                        compativel = false;
                    }
                }
                verificaTipo.Clear();
                return compativel;
            }
            else
            {
                TrataErroSemantico("ERRO: Variavel não declarada");
            }
            verificaTipo.Clear();
            return compativel;


        }

        public bool Verifica_booleano() //Verificar inteiro com operdor e / ou
        {
            int ret, indice;
            string expressao = "";
            bool compativel = false, opLogico = false, continua = true, continuaBool = true;
            List<string> aux1 = new List<string>();
            List<bool> listaBool = new List<bool>();

            indice = 0;

            for (int i = 0; i < sintatico.auxExpressao.Count; i++)
            {

                if (sintatico.auxExpressao[i] != "e" && sintatico.auxExpressao[i] != "ou")
                    aux1.Add(sintatico.auxExpressao[i]);




                if (sintatico.auxExpressao[i] == "e" || sintatico.auxExpressao[i] == "ou" || i + 1 == sintatico.auxExpressao.Count)
                {
                    listaBool.Add(RetornaTipo_Expressao(aux1));
                    aux1.Clear();

                }
            }


            for (int i = 0; i < listaBool.Count; i++)
            {

                if (listaBool[i] == true)
                {
                    compativel = true;
                }
                else
                {
                    compativel = false;
                    break;
                }

            }

            return compativel;

        }

        public bool RetornaTipo_Expressao(List<string> auxExpressao)
        {
            int ret, indice;
            bool compativel = false, opLogico = false, continua = true, continuaBool = true;
            List<string> aux1 = new List<string>();
            //indice = 0;

            if (auxExpressao.Count == 1 || (auxExpressao.Count == 3 && (auxExpressao[0] == "(" && auxExpressao[2] == ")")))
            {

                if (auxExpressao.Count == 1)
                {
                    ret = Pesquisa_tabela(auxExpressao[0], sintatico.nivelEscopo);
                    if (ret != -1)
                    {
                        if (tabelaSimbolos[ret].tipo == "booleano" || tabelaSimbolos[ret].tipo == "ooleano")
                        {
                            compativel = true;
                        }
                        else
                        {
                            compativel = false;
                        }
                    }

                }
                else
                {
                    ret = Pesquisa_tabela(auxExpressao[1], sintatico.nivelEscopo);
                    if (ret != -1)
                    {
                        if (tabelaSimbolos[ret].tipo == "booleano" || tabelaSimbolos[ret].tipo == "ooleano")
                        {
                            compativel = true;
                        }
                        else
                        {
                            compativel = false;
                        }
                    }
                }


            }
            else
            {

                for (int i = 0; i < auxExpressao.Count; i++) // trata caso tipo a+z com z booleano
                {
                    if (auxExpressao[i] == "-" || auxExpressao[i] == "+" || auxExpressao[i] == "*" || auxExpressao[i] == "div")
                    {
                        if (!GeraCodigo.geracodigo.isOperator(auxExpressao[i - 1]) && !GeraCodigo.geracodigo.isOperator(auxExpressao[i + 1])) // ex a+b
                        {
                            ret = Pesquisa_tabela(auxExpressao[i - 1], sintatico.nivelEscopo);
                            if (ret != -1)
                            {
                                if (tabelaSimbolos[ret].tipo == "booleano" || tabelaSimbolos[ret].tipo == "ooleano")
                                {
                                    continuaBool = false;
                                    break;
                                }
                            }

                            ret = Pesquisa_tabela(auxExpressao[i + 1], sintatico.nivelEscopo);
                            if (ret != -1)
                            {
                                if (tabelaSimbolos[ret].tipo == "booleano" || tabelaSimbolos[ret].tipo == "ooleano")
                                {
                                    continuaBool = false;
                                    break;
                                }
                            }

                        }

                    }
                    else
                    {
                        continuaBool = true;
                    }
                }



                for (int i = 0; i < auxExpressao.Count; i++)
                {
                    if (auxExpressao[i] == "e" || auxExpressao[i] == "ou")
                    {
                        opLogico = true;
                        break;
                    }
                    else
                    {
                        opLogico = false;
                    }
                }

                if (opLogico)
                {
                    for (int i = 0; i < auxExpressao.Count; i++)
                    {
                        ret = Pesquisa_tabela(auxExpressao[i], sintatico.nivelEscopo);
                        if (ret != -1)
                        {
                            if (tabelaSimbolos[ret].tipo == "inteiro" || tabelaSimbolos[ret].tipo == "nteiro")
                            {
                                continua = false;
                                compativel = false;
                                break;
                            }
                            else
                            {
                                continua = true;
                                compativel = true;
                            }


                        }
                        else
                        {
                            if (int.TryParse(auxExpressao[i], out int g))
                            {
                                continua = false;
                                compativel = false;
                                break;
                            }
                        }

                    }
                }

                if (continua && continuaBool)
                {
                    for (int i = 0; i < auxExpressao.Count; i++)
                    {


                        ret = Pesquisa_tabela(auxExpressao[i], sintatico.nivelEscopo);
                        if (ret != -1 || int.TryParse(auxExpressao[i], out int g))
                        {

                            if (ret != -1)
                            {
                                if (tabelaSimbolos[ret].tipo == "inteiro" || tabelaSimbolos[ret].tipo == "nteiro" || tabelaSimbolos[ret].tipo == "booleano" || tabelaSimbolos[ret].tipo == "ooleano")
                                {
                                    compativel = true;
                                }
                                else
                                {
                                    compativel = false;
                                    break;
                                }
                            }
                            else
                            {

                                if (int.TryParse(auxExpressao[i], out int j))
                                {
                                    compativel = true;
                                }
                                else
                                {
                                    compativel = false;
                                    break;
                                }
                            }

                        }


                    }

                    if (compativel)
                    {
                        for (int i = 0; i < auxExpressao.Count; i++)
                        {
                            if (Verifica_operador_booleano(auxExpressao[i]))
                            {
                                compativel = true;
                                break;
                            }
                            else
                            {
                                compativel = false;
                            }

                        }
                    }

                }

            }

            return compativel;

        }

        public bool Verifica_operador_booleano(string ch)
        {
            if (ch == ">" || ch == "<" || ch == "<=" || ch == ">=" || ch == "=" || ch == "!=" || ch == "e" || ch == "ou")
            {
                return true;
            }
            else
            {
                return false;
            }
        }



    }




}