﻿/*Mateus Talzzia
 * Paulo Oliveira*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Numerics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.FileIO;
using System.Windows.Input;
using System.Threading;

namespace MaquinaVirtual
{
    public partial class Form1 : Form
    {
        private int[] M = new int[1024]; //Ver o tamanho da pilha
        private int[] P = new int[1024]; //Ver o tamanho da pilha
        private int s = -1, i = 0;
        private int contLinha;
        private int passoApassoCount = 1;
        private Boolean proximoBp = true;
        List<int> breakpoints = new List<int>();
        private int contCall = 0;
        List<int> listReturns = new List<int>();
        List<int> listConts = new List<int>();
        List<int> listReturnfs = new List<int>();

        private Boolean returnAtivo = false;
        private Boolean returnfAtivo = false;
        private int linhaReturn = 0;


        public Form1()
        {
            InitializeComponent();
            KeyPreview = true;
        }


        private void splitContainer1_Panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void arquivoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            contLinha = 0;
            dataGridView1.Rows.Clear();
            dataGridView2.Rows.Clear();
            dataGridView3.Rows.Clear();
            dataGridView4.Rows.Clear();
            dataGridView5.Rows.Clear();
            s = -1;
            i = 0;
            button1.Enabled = false;


            for (int i = 0; i < M.Length; i++)
            {
                M[i] = 0;
            }

            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                /* System.IO.StreamReader sr = new System.IO.StreamReader(openFileDialog1.FileName);
                 MessageBox.Show(sr.ReadToEnd());  */


                using (TextFieldParser parser = new TextFieldParser(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, openFileDialog1.FileName)))
                {
                    // TextFieldParser parser2 = parser;
                    parser.Delimiters = new string[] { " ", "," };
                    while (true)
                    {
                        string[] LineData = parser.ReadFields();


                        if (LineData == null)
                        {
                            break;
                        }
                        Console.WriteLine("{0} field(s)", LineData.Length);

                        if (LineData[0].Equals("ALLOC") || LineData[0].Equals("DALLOC") || LineData[0].Equals("RETURNF"))
                        {
                            dataGridView1.Rows.Add(new object[] { contLinha, LineData[0], LineData[1], LineData[2] });
                        }
                        else if (LineData[0].Equals("LDC") || LineData[0].Equals("LDV") || LineData[0].Equals("STR") || LineData[0].Equals("JMP") || LineData[0].Equals("JMPF") || LineData[0].Equals("CALL") || int.TryParse(LineData[0],out int r))
                        {

                            dataGridView1.Rows.Add(new object[] { contLinha, LineData[0], LineData[1] });
                        }
                        else
                        {
                            dataGridView1.Rows.Add(new object[] { contLinha, LineData[0] });
                        }
                        contLinha++;
                    }

                }
                executarToolStripMenuItem.Enabled = true;
                //dataGridView3.Rows.Add(dataGridView1.Rows[1].Cells[2].Value);

                //MessageBox.Show(dataGridView1.Rows[1].Cells[2].Value.ToString());

                //sr.Close();
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void executarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void passoAPassoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            button1.Enabled = true;
            passoApassoCount = 1;
            for(int i=0;i<M.Length;i++)
            {
                M[i] = 0;
            }
                  
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows[passoApassoCount-1].Selected = false;
            dataGridView1.Rows[passoApassoCount].Selected = true;
            logicaInstrucoes();
            passoApassoCount++;


        }

        private void breakpointToolStripMenuItem_Click(object sender, EventArgs e)
        {
           passoApassoCount = 1;

            if (breakpoints.Count != 0)
            {
                int bp = breakpoints.First();

                do {
                    proximoBp = false;
                    dataGridView1.Rows[passoApassoCount - 1].Selected = false;
                    dataGridView1.Rows[passoApassoCount].Selected = true;
                    logicaInstrucoes();
                    passoApassoCount++;
                } while ((passoApassoCount <= bp));

                breakpoints.RemoveAt(0);
            }
            
        }

        private void normalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(dataGridView1.Rows[0].Cells[1].Value.ToString().Equals("START"))
            {
                
                for(passoApassoCount = 1; passoApassoCount < contLinha; passoApassoCount++)
                {
                    logicaInstrucoes();

                }
            }
            else
            {
                MessageBox.Show("Programa sem a instrução START","ERRO",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
            dataGridView1.ClearSelection();
        }

        void logicaInstrucoes()
        {

            if (dataGridView1.Rows[0].Cells[1].Value.ToString().Equals("START"))
            {
                switch (dataGridView1.Rows[passoApassoCount].Cells[1].Value.ToString())
                {
                    case "ALLOC":
                        int m, k, n;

                        m = Int32.Parse(dataGridView1.Rows[passoApassoCount].Cells[2].Value.ToString());
                        n = Int32.Parse(dataGridView1.Rows[passoApassoCount].Cells[3].Value.ToString());



                        for (k = 0; k <= n - 1; k++)
                        {
                            s = s + 1;
                            M[s] = M[m + k];
                            dataGridView2.Rows.Add(s, M[m + k]);
                        }


                        break;

                    case "START":
                        s = -1;
                       
                        
                        break;



                    case "LDC":

                        n = Int32.Parse(dataGridView1.Rows[passoApassoCount].Cells[2].Value.ToString());

                        s = s + 1;
                        M[s] = n; //teoricamente "n" é "k"


                        for (int procura = 0; procura < dataGridView2.RowCount - 1; procura++)
                        {
                            //Console.WriteLine("procura {0}, procuravalue {1}, s {2}", procura, dataGridView2.Rows[procura].Cells[0].Value.ToString(), s);
                            if (dataGridView2.Rows[procura].Cells[0].Value.ToString().Equals(s.ToString()))
                            {
                                dataGridView2.Rows.RemoveAt(procura);
                                //MessageBox.Show(procura.ToString());

                                break;
                            }
                        }
                        dataGridView2.Rows.Add(s, n);
                        break;

                    case "LDV":

                        n = Int32.Parse(dataGridView1.Rows[passoApassoCount].Cells[2].Value.ToString());


                        s = s + 1;
                        M[s] = M[n];
                        for (int procura = 0; procura < dataGridView2.RowCount - 1; procura++)
                        {
                            if (dataGridView2.Rows[procura].Cells[0].Value.ToString().Equals(s.ToString()))
                            {
                                dataGridView2.Rows.RemoveAt(procura);

                                break;
                            }
                        }
                        dataGridView2.Rows.Add(s, M[n]);
                        break;

                    case "ADD":


                        M[s - 1] = M[s - 1] + M[s];
                    
                        dataGridView2.Rows.RemoveAt(s - 1);
                        dataGridView2.Rows.Add(s - 1, M[s - 1]);
                    
                        s = s - 1;

                        break;

                    case "SUB":

                        M[s - 1] = M[s - 1] - M[s];

                        dataGridView2.Rows.RemoveAt(s - 1);
                        dataGridView2.Rows.Add(s - 1, M[s - 1]);
                        s = s - 1;
                        break;

                    case "MULT":

                        M[s - 1] = M[s - 1] * M[s];
                        dataGridView2.Rows.RemoveAt(s - 1);
                        dataGridView2.Rows.Add(s - 1, M[s - 1]);
                        s = s - 1;
                        break;

                    case "DIVI":

                        M[s - 1] = M[s - 1] / M[s];
                        dataGridView2.Rows.RemoveAt(s - 1);
                        dataGridView2.Rows.Add(s - 1, M[s - 1]);
                        s = s - 1;
                        break;

                    case "INV":

                        M[s] = -M[s];
                        dataGridView2.Rows.RemoveAt(s - 1);
                        dataGridView2.Rows.Add(s - 1, M[s - 1]);
                        dataGridView2.Rows.Add(s, M[s].ToString());
                        break;

                    case "AND":

                        if (M[s - 1] == 1 && M[s] == 1)
                        {
                            M[s - 1] = 1;

                        }
                        else
                        {
                            M[s - 1] = 0;
                        }
                        dataGridView2.Rows.Add(s - 1, M[s - 1].ToString());

                        s = s - 1;
                        break;

                    case "PRN":
                        dataGridView3.Rows.Add(M[s].ToString());
                        dataGridView2.Rows.RemoveAt(s);

                        s = s - 1;
                        break;

                    case "OR":

                        if (M[s - 1] == 1 || M[s] == 1)
                        {
                            M[s - 1] = 1;

                        }
                        else
                        {
                            M[s - 1] = 0;

                        }
                        dataGridView2.Rows.Add(s - 1, M[s - 1].ToString());

                        s = s - 1;
                        break;

                    case "NEG":

                        M[s] = 1 - M[s];
                        dataGridView2.Rows.Add(s, M[s].ToString());

                        break;

                    case "CME":
                        if (M[s - 1] < M[s])
                        {
                            M[s - 1] = 1;

                        }
                        else
                        {
                            M[s - 1] = 0;

                        }
                        dataGridView2.Rows.Add(s - 1, M[s - 1].ToString());

                        s = s - 1;
                        break;

                    case "CMA":
                        if (M[s - 1] > M[s])
                        {
                            M[s - 1] = 1;
                        }
                        else
                        {
                            M[s - 1] = 0;
                        }
                        dataGridView2.Rows.RemoveAt(s);


                        s = s - 1;
                        break;

                    case "CEQ":
                        if (M[s - 1] == M[s])
                        {
                            M[s - 1] = 1;
                        }
                        else
                        {
                            M[s - 1] = 0;
                        }
                        dataGridView2.Rows.RemoveAt(s);

                        s = s - 1;
                        break;

                    case "CDIF":
                        if (M[s - 1] != M[s])
                        {
                            M[s - 1] = 1;
                        }
                        else
                        {
                            M[s - 1] = 0;
                        }
                        dataGridView2.Rows.RemoveAt(s);

                        s = s - 1;
                        break;

                    case "CMEQ":
                        if (M[s - 1] <= M[s])
                        {
                            M[s - 1] = 1;
                        }
                        else
                        {
                            M[s - 1] = 0;
                        }
                        dataGridView2.Rows.RemoveAt(s);

                        s = s - 1;
                        break;

                    case "CMAQ":
                        if (M[s - 1] >= M[s])
                        {
                            M[s - 1] = 1;
                        }
                        else
                        {
                            M[s - 1] = 0;
                        }
                        dataGridView2.Rows.RemoveAt(s);

                        s = s - 1;
                        break;

                    case "HLT":
                        button1.Enabled = false;
                        break;

                    case "STR":
                        n = Int32.Parse(dataGridView1.Rows[passoApassoCount].Cells[2].Value.ToString());
                        dataGridView2.Rows.Add(n, M[s]);

                        M[n] = M[s];
                        s = s - 1;
                        break;

                    case "JMP":
                        for (int linha = 0; linha < contLinha; linha++)
                        {
                            // Console.WriteLine("Cells2: {0}", dataGridView1.Rows[passoApassoCount].Cells[2].Value.ToString());
                            // Console.WriteLine("Cells1: {0}", dataGridView1.Rows[linha].Cells[1].Value.ToString());
                            if (dataGridView1.Rows[passoApassoCount].Cells[2].Value.ToString().Equals(dataGridView1.Rows[linha].Cells[1].Value.ToString()))
                            {
                                dataGridView1.Rows[passoApassoCount].Selected = false;
                                dataGridView1.Rows[linha].Selected = true;
                                passoApassoCount = linha;
                                break;
                            }
                        }

                        break;

                    case "JMPF":


                        if (M[s] == 0)
                        {
                            for (int linha = 0; linha < contLinha; linha++)
                            {
                                Console.WriteLine("Cells2: {0}", dataGridView1.Rows[passoApassoCount].Cells[2].Value.ToString());
                                Console.WriteLine("Cells1: {0}", dataGridView1.Rows[linha].Cells[1].Value.ToString());
                                if (dataGridView1.Rows[passoApassoCount].Cells[2].Value.ToString().Equals(dataGridView1.Rows[linha].Cells[1].Value.ToString()))
                                {
                                    dataGridView1.Rows[passoApassoCount].Selected = false;
                                    dataGridView1.Rows[linha].Selected = true;
                                    passoApassoCount = linha;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            //passoApassoCount++;
                        }
                        s = s - 1;
                        break;

                    case "NULL":
                        break;

                    case "RD": 
                        s = s + 1;
                        string valor;
                        
                        valor = Interaction.InputBox("Entrada de dados requerida", "Instrução RD encontrada", "");

                        
                        if(valor != "")
                        {
                            M[s] = Int32.Parse(valor);
                            dataGridView5.Rows.Add(valor);
                            dataGridView2.Rows.Add(s, valor);
                        }
                        else
                        {
                            passoApassoCount++;
                        }

                        break;

                    case "DALLOC":
                        m = Int32.Parse(dataGridView1.Rows[passoApassoCount].Cells[2].Value.ToString());
                        n = Int32.Parse(dataGridView1.Rows[passoApassoCount].Cells[3].Value.ToString());

                        for (k = (n - 1); k >= 0; k--)
                        {

        
                            if(s>=0)
                            {
                                M[m + k] = M[s];
                                //s = s - 1;

                            }
                            s = s - 1;

                        }

                        break;

                    case "CALL":
                      

                        for (int linha = 0; linha < contLinha; linha++)
                        {
                           
                            if (dataGridView1.Rows[passoApassoCount].Cells[2].Value.ToString().Equals(dataGridView1.Rows[linha].Cells[1].Value.ToString()))
                            {
                                dataGridView1.Rows[passoApassoCount].Selected = false;
                                dataGridView1.Rows[linha].Selected = true;
                                s = s + 1;
                                M[s] = passoApassoCount + 1;
                                dataGridView2.Rows.Add(s, M[s]);



                                passoApassoCount = linha;
                                
                                break;
                            }
                        }
                 
                        break;

                    case "RETURN":
                        
                        dataGridView1.Rows[passoApassoCount].Selected = false;
                        passoApassoCount = M[s]-1;
                        dataGridView1.Rows[passoApassoCount+1].Selected = true;

                        s = s - 1;
                          
                        if(dataGridView1.Rows[passoApassoCount+1].Cells[1].Value.ToString().Equals("HLT"))
                        {
                            button1.Enabled = false;
                        }

                
                        break;

                    case "RETURNF":

                        int valorRetorno;
                        bool fazDalloc = true;

                        m = 0;
                        n = 0;
                            
                        if(int.TryParse((dataGridView1.Rows[passoApassoCount].Cells[2].Value.ToString()),out int g))
                        {
                            m = Int32.Parse(dataGridView1.Rows[passoApassoCount].Cells[2].Value.ToString());
                            n = Int32.Parse(dataGridView1.Rows[passoApassoCount].Cells[3].Value.ToString());
                            fazDalloc = true;
                        }
                        else
                        {
                            fazDalloc = false;
                        }

                       

                        valorRetorno = M[s]; //Guarda topo da pilha em uma auxiliar

                        s = s - 1; //Decrementa topo da pilha
                        if(fazDalloc)
                        {
                            //DALLOC
                            for (k = (n - 1); k >= 0; k--)
                            {

                                dataGridView2.Rows.RemoveAt(s);

                                if (s >= 0)
                                {
                                    M[m + k] = M[s];

                                }
                                s = s - 1;


                            }

                        }
                        

                        //RETURN
                        dataGridView1.Rows[passoApassoCount].Selected = false;
                        passoApassoCount = M[s]-1;
                        dataGridView1.Rows[passoApassoCount+1].Selected = true;

                        s = s - 1;
  
                        if (dataGridView1.Rows[passoApassoCount+1].Cells[1].Value.ToString().Equals("HLT"))
                        {
                            button1.Enabled = false;
                        }


                        //LDC
                        s = s + 1;
                        M[s] = valorRetorno;
                        
                     
                        break;
                }



            }
            else
            {
                MessageBox.Show("Programa sem a instrução START", "ERRO", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        protected override void OnKeyDown(KeyEventArgs e)
        {
            if(e.KeyCode == Keys.F9)
            {
                int index = dataGridView1.CurrentRow.Index;

                dataGridView4.Rows.Add(index.ToString());
                breakpoints.Add(index);
                button1.Enabled = true;
            }
            if (e.KeyCode == Keys.Enter)
            {
                if(passoApassoCount != contLinha) { 
                    if (breakpoints.Count != 0)
                    {
                        int bp = breakpoints.First();
                        do
                        {
                            proximoBp = false;

                            dataGridView1.Rows[passoApassoCount - 1].Selected = false;
                            dataGridView1.Rows[passoApassoCount].Selected = true;
                            logicaInstrucoes();
                            passoApassoCount++;

                        } while ((passoApassoCount <= bp));
                        breakpoints.RemoveAt(0);
                    }
                    else
                    {
                        do
                        {
                            logicaInstrucoes();
                            passoApassoCount++;
                        } while (passoApassoCount < contLinha);
                        dataGridView1.ClearSelection();
                    }
            }
                //MessageBox.Show("Enter");
            }
            e.Handled = true;
        }
    }
}